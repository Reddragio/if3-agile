package com.insa.h4112.controller;

/**
 * Class that contains all events names produced with property change support
 *
 * @author Pierre-Yves GENEST
 */
public class Events {

    /**
     * When a checkpoint is selected
     * associated values :
     * old : null
     * new : Checkpoint
     */
    public static final String SELECT_CHECKPOINT_EVENT = "SELECT_CHECKPOINT_EVENT";

    /**
     * When a checkpoint is reordered (in a tour)
     * old : null
     * new : Tour
     */
    public static final String REORDER_CHECKPOINT_EVENT = "REORDER_CHECKPOINT";

    /**
     * When a delivery is added
     */
    public static final String ADD_DELIVERY_FROM_TOUR_EVENT = "ADD_DELIVERY_FROM_TOUR";
    public static final String ADD_DELIVERY_FROM_TOUR_REQUEST_EVENT = "ADD_DELIVERY_FROM_TOUR_REQUEST";

    /**
     * When a delivery is removed
     */
    public static final String REMOVE_DELIVERY_FROM_TOUR_EVENT = "REMOVE_DELIVERY";
    public static final String REMOVE_DELIVERY_FROM_TOUR_REQUEST_EVENT = "REMOVE_DELIVERY_FROM_TOUR_REQUEST";

    /**
     * When a intersection is selected
     * associated values :
     * old : null
     * new : intersection
     */
    public static final String SELECT_INTERSECTION_EVENT = "SELECT_INTERSECTION_EVENT";

    /**
     * To send whether interval contains passage time or not (specific to checkpoint)
     * old : null
     * new : if contains
     */
    public static final String CHECKPOINT_INTERVAL_CONTAINING_PASSAGE_TIME_EVENT = "UPDATE_INTERVAL_CONTAINING_PASSAGE_TIME";

    /**
     * When component needs to repaint
     */
    public static final String REPAINT_EVENT = "REPAINT";

    /**
     * Deleted constructor
     */
    private Events() {
    }
}
