package com.insa.h4112.controller.command.util;

import com.insa.h4112.controller.command.Command;

/**
 * A Command History is simply a hierarchical list of commands.
 * The CommandHistory has a current sub history on which it does the "undo" and "redo".
 * The user can add or remove a sub history at the end of the histories list.
 * A CommandHistory always keeps a minimum of one sub history.
 *
 * @author Martin FRANCESCHI
 */
public interface CommandHistory {
    /**
     * Add a command to command list AND execute it.
     *
     * @param command command to add.
     */
    void addCommand(Command command);

    /**
     * Try to undo previous command.
     *
     * @return true if undo succeeded
     */
    boolean undoCommand();

    /**
     * Redo next command.
     *
     * @return true if redo succeeded
     */
    boolean redoCommand();

    /**
     * Add a new empty sub history (for sharper undo/redo).
     * The current history now becomes the current sub history.
     */
    void addSubHistory();

    /**
     * Delete current sub history.
     * The current history now becomes the previous sub history.
     * Calling it does nothing if it would result in no current sub history.
     *
     * @return true if a sub history was effectively deleted.
     */
    boolean popSubHistory();
}

