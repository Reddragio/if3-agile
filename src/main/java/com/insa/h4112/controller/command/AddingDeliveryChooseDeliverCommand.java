package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.controller.state.AddingDeliveryDeliveryChosenState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.TourRequest;

/**
 * Command to add the deliver checkpoint when adding delivery
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryChooseDeliverCommand extends StateTransitionCommand {
    /**
     * Main constructor
     *
     * @param controller             The app's controller.
     * @param previousState          The previous state.
     * @param pickupCheckpoint       The pickup checkpoint of the delivery to add.
     * @param checkpointBeforePickup The chosen checkpoint to go by before the pickup checkpoint of the added delivery.
     * @param deliverIntersection    The intersection chosen to be the deliver checkpoint of the delivery to add.
     */
    public AddingDeliveryChooseDeliverCommand(Controller controller, State previousState, Checkpoint pickupCheckpoint, Checkpoint checkpointBeforePickup, Intersection deliverIntersection) throws CommandException {
        super(controller, previousState);

        TourCompleteGraph tourCompleteGraph = controller.getTourCompleteGraph();
        TourRequest tourRequest = controller.getTourRequest();
        if (!tourCompleteGraph.verifyCheckpointReachableFromWarehouse(tourRequest.getWarehouse(), pickupCheckpoint)) {
            throw new CommandException("Le point de livraison n'est pas atteignable");
        }

        targetState = new AddingDeliveryDeliveryChosenState(controller.getMap(), controller.getTour(), tourRequest, tourCompleteGraph, pickupCheckpoint, checkpointBeforePickup, deliverIntersection);
    }

    @Override
    public void doCommand() {
        super.doCommand();
    }
}
