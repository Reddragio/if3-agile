package com.insa.h4112.controller.command.util;

import com.insa.h4112.controller.command.Command;
import com.insa.h4112.util.log.Log;

import java.util.Stack;

/**
 * Implementation of the CommandHistory interface with a stack of CommandLists.
 *
 * @author Martin FRANCESCHI
 * @author Pierre-Yves GENEST
 */
public class StackCommandHistory implements CommandHistory {

    /**
     * History (as a list) of commands run by the controller.
     * Stack is used to allow sharper granularity of undo/redo
     */
    private Stack<CommandList> histories = new Stack<>();

    /**
     * Single constructor. Creates a sub history.
     */
    public StackCommandHistory() {
        addSubHistory();
    }

    /**
     * Add a command to command list AND execute it.
     *
     * @param command command to add.
     */
    @Override
    public void addCommand(Command command) {
        if (command != null) {
            histories.lastElement().add(command);
        }
    }

    /**
     * Try to undo previous command.
     *
     * @return true if undo succeeded
     */
    @Override
    public boolean undoCommand() {
        boolean undoResult = histories.lastElement().undo();

        // Pop every empty sub history
        while (histories.lastElement().isEverythingUndone()) {
            if (!popSubHistory()) {
                // The current sub history is the last remaining.
                break;
            }
        }

        return undoResult;
    }

    /**
     * Redo next command.
     *
     * @return true if redo succeeded
     */
    @Override
    public boolean redoCommand() {
        return histories.lastElement().redo();
    }

    /**
     * Add a new empty sub history (for sharper undo/redo).
     * The current history now becomes the current sub history.
     */
    @Override
    public void addSubHistory() {
        histories.push(new CommandList());
    }

    /**
     * Delete current sub history.
     * The current history now becomes the previous sub history.
     * Calling it does nothing if it would result in no current sub history.
     *
     * @return true if a sub history was effectively deleted.
     */
    @Override
    public boolean popSubHistory() {
        if (histories.size() > 1) {
            histories.pop();
            Log.debug("Suppression d'une Command List", this.getClass());
            return true;
        } else {
            Log.debug("Echec de la suppression d'une Command List", this.getClass());
            return false;
        }
    }
}
