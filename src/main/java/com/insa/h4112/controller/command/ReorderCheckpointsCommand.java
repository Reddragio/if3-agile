package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.domain.Tour;

/**
 * Command to reorder the elements in the table, a deliver checkpoint must be after the pickup checkpoint
 *
 * @author Jacques CHARNAY
 */
public class ReorderCheckpointsCommand implements Command {
    /**
     * Index of the checkpoint to move
     */
    private int from;

    /**
     * Index where the moved checkpoint must go
     */
    private int to;

    /**
     * The tour
     */
    private Tour tour;

    /**
     * The controller
     */
    private Controller controller;

    /**
     * Command reordering the checkpoints of the tour
     *
     * @param controller Controller of the application.
     * @param from       The index of checkpoint to change.
     * @param to         The chosen new index of checkpoint.
     * @param tour       The tour.
     * @throws CommandException
     */
    public ReorderCheckpointsCommand(Controller controller, int from, int to, Tour tour) throws CommandException {
        this.from = from;
        this.to = to;
        this.tour = tour;
        this.controller = controller;
        if (!tour.getMoveCheckpointFromToAllowed(from, to)) {
            throw new CommandException("Réordonnancement impossible !");
        }
    }

    @Override
    public void doCommand() {
        tour.moveCheckpointFromTo(from, to);
    }

    @Override
    public void undoCommand() {
        if (to <= from) {
            tour.moveCheckpointFromTo(to, from + 1);
        } else {
            tour.moveCheckpointFromTo(to - 1, from);
        }
    }
}
