package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.state.OptimizedTourState;
import com.insa.h4112.controller.state.OptimizingTourState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.algorithm.TSPEvo;
import com.insa.h4112.model.algorithm.TemplateTSPEvo;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.util.log.Log;

import javax.swing.*;
import java.util.concurrent.ExecutionException;

/**
 * Command to compute the optimized tour.
 * It sets the GUI in OptimizingTourState and asks a dedicated thread to do the computations.
 * When the thread is finished the GUI becomes in OptimizedTourState.
 * This command can only be undone if the work is finished or of timeout is over.
 *
 * @author Martin FRANCESCHI
 * @author Paul Goux
 * @author Jacques CHARNAY
 */
public class ComputeOptimizedTourCommand extends StateTransitionCommand {

    /**
     * Map of the application
     */
    private Map map;

    /**
     * Tour request of the application
     */
    private TourRequest tourRequest;

    /**
     * The tour request resolved by the TSP algorithm
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * The TSP algorithm to optimize tour
     */
    private TemplateTSPEvo templateTSPEvo;

    /**
     * The tour of the application
     */
    private Tour tour;

    /**
     * Starting time to check if the timeout has been reached
     */
    private long millisecondsBeginning;

    /**
     * Constructor
     *
     * @param controller    controller
     * @param previousState the previous state
     */
    public ComputeOptimizedTourCommand(Controller controller, State previousState) {
        super(controller, previousState);
        map = controller.getMap();
        tourRequest = controller.getTourRequest();
        tourCompleteGraph = controller.getTourCompleteGraph();
        templateTSPEvo = new TSPEvo(tourRequest, tourCompleteGraph);
        targetState = new OptimizingTourState(map, tourRequest, tourCompleteGraph);

        Log.beginOptimization();
        CalculationSwingWorker calculationSwingWorker = new CalculationSwingWorker();
        calculationSwingWorker.execute();
    }

    /**
     * Tells whether the task is finished.
     * We have to use something different than `SwingWorker.isDone()`
     * because the new state is set during the `SwingWorker.done()` method.
     *
     * @return true if the task if finished.
     */
    private boolean isCalculationDone() {
        return targetState instanceof OptimizedTourState;
    }

    @Override
    public void undoCommand() throws UnsupportedOperationException {
        // Undo only if the job is over.
        if (isCalculationDone())
            controller.changeState(previousState);
        else
            throw new UnsupportedOperationException();
    }

    /**
     * Class that handles the computing of the optimized tour.
     * This Swing Worker works on an AWT-specific thread and thus can work on the GUI.
     */
    private class CalculationSwingWorker extends SwingWorker<Tour, Void> {
        @Override
        protected Tour doInBackground() {
            millisecondsBeginning = System.currentTimeMillis();

            // Compute
            if (tour == null) {
                return templateTSPEvo.searchSolution(30000);
            } else {
                return tour;
            }
        }

        @Override
        protected void done() {
            // Get the tour.
            try {
                tour = get();
            } catch (InterruptedException | ExecutionException e) {
                Log.debug(e.getMessage());
            }
            long millisecondsEnding = System.currentTimeMillis();

            // Log the end.
            Log.successOptimizeTour(
                    !templateTSPEvo.getTimeLimitReached(),
                    millisecondsEnding - millisecondsBeginning);

            // Set the new state and frees the GUI.
            if (!isCalculationDone()) {
                targetState = new OptimizedTourState(map, tourRequest, tourCompleteGraph, tour);
                controller.changeState(targetState);
            }
        }
    }

}
