package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.controller.state.MapState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.parser.MapXMLParser;
import com.insa.h4112.util.log.Log;
import org.jdom2.JDOMException;

import java.io.File;
import java.io.IOException;

/**
 * This command changes the controller's state from any previous to a new Map State.
 *
 * @author Pierre-Yves GENEST
 * @author Jacques CHARNAY
 * @author Martin FRANCESCHI
 */
public class LoadMapCommand extends StateTransitionCommand {

    /**
     * Constructor
     *
     * @param controller    Application's controller
     * @param previousState previous state of controller
     * @throws CommandException if cannot load map
     */
    public LoadMapCommand(Controller controller, State previousState) throws CommandException {
        super(controller, previousState);

        // Load map
        File chosenFile = controller.getMainWindow().selectXMLFile();
        if (chosenFile == null) { // file is invalid
            throw new CommandException(Log.ERROR_COMMAND_NO_FILE_SELECTED);
        }

        try {
            Map map = MapXMLParser.parseMap(chosenFile);
            targetState = new MapState(map);
            Log.successLoadMap(chosenFile);
        } catch (JDOMException e) {
            throw new CommandException(Log.ERROR_COMMAND_XML_INVALID);
        } catch (IOException e) {
            throw new CommandException(Log.ERROR_COMMAND_XML_NOT_FOUND);
        }
    }
}