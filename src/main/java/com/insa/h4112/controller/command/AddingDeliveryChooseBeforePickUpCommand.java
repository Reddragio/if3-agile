package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.state.AddingDeliveryBeforePickUpChosenState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Intersection;

import java.time.Duration;

/**
 * Command to add the checkpoint to go by before the pickup checkpoint of the added delivery
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryChooseBeforePickUpCommand extends StateTransitionCommand {
    /**
     * Main constructor.
     *
     * @param controller             The app's controller.
     * @param previousState          The previous state.
     * @param pickupIntersection     The intersection chosen to be the pickup checkpoint of the delivery to add.
     * @param checkpointBeforePickup The chosen checkpoint to go by before the pickup checkpoint of the added delivery.
     * @param duration               The duration of the pickup.
     */
    public AddingDeliveryChooseBeforePickUpCommand(Controller controller, State previousState, Intersection pickupIntersection, Checkpoint checkpointBeforePickup, Duration duration) {
        super(controller, previousState);
        targetState = new AddingDeliveryBeforePickUpChosenState(controller.getMap(), controller.getTour(), controller.getTourRequest(), controller.getTourCompleteGraph(), new Checkpoint(duration, pickupIntersection), checkpointBeforePickup);
    }

    @Override
    public void doCommand() {
        super.doCommand();
    }
}
