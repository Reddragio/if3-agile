package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.controller.state.OptimizedTourState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Delivery;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.util.log.Log;

/**
 * Command to add the delivery to the tour
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddDeliveryCommand extends StateTransitionCommand {

    private Controller controller;

    private Delivery delivery;

    private Checkpoint beforePickupCheckpoint;

    private Checkpoint beforeDeliverCheckpoint;

    /**
     * Main constructor
     *
     * @param controller              The app's controller.
     * @param previousState           The previous state.
     * @param delivery                The delivery to add
     * @param beforePickupCheckpoint  The chosen checkpoint to go by before the pickup checkpoint of the added delivery
     * @param beforeDeliverCheckpoint The chosen checkpoint to go by before the deliver checkpoint of the added delivery
     */
    public AddDeliveryCommand(Controller controller, State previousState, Delivery delivery, Checkpoint beforePickupCheckpoint, Checkpoint beforeDeliverCheckpoint) throws CommandException {
        super(controller, previousState);
        this.controller = controller;
        this.delivery = delivery;
        this.beforePickupCheckpoint = beforePickupCheckpoint;
        this.beforeDeliverCheckpoint = beforeDeliverCheckpoint;

        if (!controller.getTour().getAddDeliveryAllowed(delivery, controller.getTour().getCheckpoints().indexOf(beforePickupCheckpoint) + 1, controller.getTour().getCheckpoints().indexOf(beforeDeliverCheckpoint) + 1)) {
            throw new CommandException("Le point de récupération doit être avant le point de livraison.");
        }

        controller.popHistory();

        targetState = new OptimizedTourState(controller.getMap(), controller.getTourRequest(), controller.getTourCompleteGraph(), controller.getTour());
    }

    /**
     * Do command
     */
    @Override
    public void doCommand() {
        super.doCommand();
        Tour tour = controller.getTour();
        tour.addDelivery(delivery, tour.getCheckpoints().indexOf(beforePickupCheckpoint) + 1, tour.getCheckpoints().indexOf(beforeDeliverCheckpoint) + 1);
        Log.successAddDelivery();
    }

    /**
     * Undo command
     */
    @Override
    public void undoCommand() {
        Tour tour = controller.getTour();
        tour.removeDeliveryOfCheckpoint(tour.getCheckpoints().indexOf(beforePickupCheckpoint) + 1);
        previousState = new OptimizedTourState(controller.getMap(), controller.getTourRequest(), controller.getTourCompleteGraph(), controller.getTour());
        super.undoCommand();
    }
}
