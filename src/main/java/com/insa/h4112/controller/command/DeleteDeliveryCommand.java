package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.state.OptimizedTourState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Delivery;
import com.insa.h4112.util.log.Log;

/**
 * This command changes the controller's state from any previous to a new OptimizedTourState.
 *
 * @author Thomas Zhou
 * @author Paul Goux
 */
public class DeleteDeliveryCommand extends StateTransitionCommand {

    /**
     * Controller of the application
     */
    private Controller controller;

    /**
     * The selected checkpoint to delete
     */
    private Checkpoint checkpoint;

    /**
     * The index of the selected checkpoint
     */
    private int selectedCheckpointIndex;

    /**
     * The index of the associated selected checkpoint
     */
    private int associatedCheckpointIndex;

    /**
     * The delivery to delete.
     */
    private Delivery delivery;

    /**
     * Main constructor
     *
     * @param controller    Controller of the application.
     * @param previousState The previous state of the application.
     */
    public DeleteDeliveryCommand(Controller controller, State previousState) {
        super(controller, previousState);
        this.controller = controller;
        this.checkpoint = controller.getSelectedCheckpoint();

        delivery = controller.getTourRequest().getAssociatedDelivery(checkpoint);
        selectedCheckpointIndex = controller.getTour().getCheckpoints().indexOf(checkpoint);
        Checkpoint associatedCheckpoint = controller.getTourRequest().getOtherCheckpointInDelivery(checkpoint);
        associatedCheckpointIndex = controller.getTour().getCheckpoints().indexOf(associatedCheckpoint);
        targetState = new OptimizedTourState(controller.getMap(), controller.getTourRequest(), controller.getTourCompleteGraph(), controller.getTour());
    }

    @Override
    public void doCommand() {
        controller.getTour().removeDeliveryOfCheckpoint(selectedCheckpointIndex);
        super.doCommand();
        Log.successDeleteDelivery();
    }

    @Override
    public void undoCommand() {
        int pickupIndex = Math.min(selectedCheckpointIndex, associatedCheckpointIndex);
        int deliveryIndex = Math.max(selectedCheckpointIndex, associatedCheckpointIndex) - 1;
        controller.getTour().addDelivery(delivery, pickupIndex, deliveryIndex);
        super.undoCommand();
    }
}
