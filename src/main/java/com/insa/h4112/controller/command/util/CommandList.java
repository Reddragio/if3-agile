package com.insa.h4112.controller.command.util;

import com.insa.h4112.controller.command.Command;

import java.util.ArrayList;
import java.util.List;

/**
 * Command list for Command design pattern
 *
 * @author Pierre-Yves GENEST
 * @author Jacques CHARNAY
 */
public class CommandList {

    /**
     * The complete list of commands.
     */
    private List<Command> commands;

    /**
     * Index of the last executed command.
     * A value of {@code -1} indicates that there is no previous command.
     */
    private int index;


    /**
     * Initializes an empty list.
     */
    public CommandList() {
        commands = new ArrayList<>();
        index = -1;
    }

    /**
     * Add a command to the command list.
     * /!\ Every command after the current command will be removed!
     *
     * @param command command to add
     */
    public void add(Command command) {
        // Removing all next commands
        if (index + 1 < commands.size()) {
            commands.subList(index + 1, commands.size()).clear();
        }
        index++;

        commands.add(command);
        command.doCommand();
    }

    /**
     * Redo command, or nothing if no more command to execute
     *
     * @return whether a command was re-done
     */
    public boolean redo() {
        if (index + 1 < commands.size()) {
            index++;
            commands.get(index).doCommand();
            return true;
        }
        return false;
    }

    /**
     * Undo previous command, or nothing if no more command to undo
     *
     * @return whether a command was un-done
     */
    public boolean undo() {
        if (index >= 0 && index < commands.size()) {
            try {
                commands.get(index).undoCommand();
            } catch (UnsupportedOperationException ignored) {
                return false;
            }
            index--;
            return true;
        }
        return false;
    }

    /**
     * Return if every command has been undone
     *
     * @return if every command has been undone
     */
    public boolean isEverythingUndone() {
        return index < 0;
    }

    /**
     * Undo and erase previous command
     * or nothing if no more command to undo
     */
    public void undoAndEraseCommand() {
        if (index >= 0 && index < commands.size()) {
            Command temp = commands.get(index);

            // Erase command (before undo in case of exception)
            commands.remove(index);
            index--;

            //Undo command
            temp.undoCommand();
        }
    }

    /**
     * Remove all commands
     */
    public void clear() {
        index = -1;
        commands.clear();
    }
}
