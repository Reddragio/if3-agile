package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.controller.state.AddingDeliveryPickUpChosenState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.TourRequest;

/**
 * Command to add the pickup checkpoint when adding delivery
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryChoosePickUpCommand extends StateTransitionCommand {
    /**
     * @param controller         The app's controller.
     * @param previousState      The previous state.
     * @param pickupIntersection The intersection chosen to be the pickup checkpoint of the delivery to add.
     */
    public AddingDeliveryChoosePickUpCommand(Controller controller, State previousState, Intersection pickupIntersection) throws CommandException {
        super(controller, previousState);

        Checkpoint pickupCheckpoint = new Checkpoint(pickupIntersection);
        TourCompleteGraph tourCompleteGraph = controller.getTourCompleteGraph();
        TourRequest tourRequest = controller.getTourRequest();
        if (!tourCompleteGraph.verifyCheckpointReachableFromWarehouse(tourRequest.getWarehouse(), pickupCheckpoint)) {
            throw new CommandException("Le point de récupération n'est pas atteignable");
        }

        targetState = new AddingDeliveryPickUpChosenState(controller.getMap(), controller.getTour(), tourRequest, tourCompleteGraph, pickupIntersection);
    }

    @Override
    public void doCommand() {
        super.doCommand();
    }
}
