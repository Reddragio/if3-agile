package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Tour;

/**
 * Base interface for State design pattern
 *
 * @author Pierre-Yves GENEST
 */
public interface State {

    /**
     * Enter state
     * => set up state
     *
     * @param controller controller
     */
    default void enter(Controller controller) {
    }

    /**
     * Leave state
     * => dismount state
     *
     * @param controller controller
     */
    default void leave(Controller controller) {
    }

    /**
     * Event to load a map
     *
     * @param controller Controller
     * @return command (or null)
     */
    default Command loadMapEvent(Controller controller) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event to load a tour request
     *
     * @param controller Controller
     * @return command (or null)
     */
    default Command loadTourRequestEvent(Controller controller) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event to compute/optimize tour
     *
     * @param controller Controller
     * @return command (or null)
     */
    default Command optimizeTourEvent(Controller controller) {
        throw new UnsupportedOperationException();
    }

    /**
     * Event to reorder checkpoints of tour
     *
     * @param controller Controller
     * @param from       Index of the checkpoint to move
     * @param to         Index where the moved checkpoint must go
     * @param tour       The associated Tour
     * @return command (or null)
     */
    default Command moveCheckpointEvent(Controller controller, int from, int to, Tour tour) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event to obtain the Roadmap.
     *
     * @param controller The controller.
     */
    default void obtainRoadmapEvent(Controller controller) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event to obtain the duration.
     *
     * @param controller The controller.
     */
    default Command obtainDurationsEvent(Controller controller) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event when a checkpoint is clicked
     *
     * @param controller        Controller
     * @param clickedCheckpoint clicked checkpoint
     * @return command (or null)
     */
    default Command clickOnCheckpointEvent(Controller controller, Checkpoint clickedCheckpoint) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event when an intersection is clicked
     *
     * @param controller   controller
     * @param intersection intersection nearest to the click
     * @return associated command
     */
    default Command clickIntersectionEvent(Controller controller, Intersection intersection) throws CommandException {
        return null;
    }

    /**
     * Event when escape key is pressed
     *
     * @param controller Controller
     * @return null
     */
    default Command escapeKeyEvent(Controller controller) {
        return null;
    }

    /**
     * Event when delete key is pressed
     *
     * @param controller Controller
     * @return null
     */
    default Command deleteKeyEvent(Controller controller) {
        return null;
    }

    /**
     * Event to delete a checkpoint
     *
     * @param controller controller
     * @return command (or null)
     */
    default Command deleteCheckpointEvent(Controller controller) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event to add a delivery
     *
     * @param controller controller
     * @return command or null
     */
    default Command addDeliveryEvent(Controller controller) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /**
     * Event triggered when a JDialog is closed
     *
     * @param controller controller
     * @return associated Command, null if none
     */
    default Command closeJDialogEvent(Controller controller) {
        return null;
    }

    /**
     * To get name of State (equivalent of toString)
     *
     * @return State class name
     */
    default String name() {
        return getClass().getName();
    }
}
