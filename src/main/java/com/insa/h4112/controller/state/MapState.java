package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.LoadMapCommand;
import com.insa.h4112.controller.command.LoadTourRequestCommand;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.view.ButtonBarView;
import com.insa.h4112.view.MainWindow;

/**
 * Loaded map state
 *
 * @author Pierre-Yves GENEST
 * @author Jacques CHARNAY
 */
public class MapState implements State {
    /**
     * Map managed by the state
     */
    private Map map;

    /**
     * Constructor
     *
     * @param map map to manage
     */
    public MapState(Map map) {
        this.map = map;
    }

    /**
     * Enter state
     * => set up state
     *
     * @param controller controller
     */
    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(true);
        mainWindow.setLoadTourRequestButtonEnable(true);
        mainWindow.setComputeTourButtonText(ButtonBarView.OPTIMIZE_BUTTON_TEXT_NOT_OPTIMIZED);
        mainWindow.setLegendVisible(false);
    }

    /**
     * Leave state
     * => cleaning for next state
     *
     * @param controller controller
     */
    @Override
    public void leave(Controller controller) {
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(false);
        mainWindow.setLoadTourRequestButtonEnable(false);
        mainWindow.setLegendVisible(true);
    }

    /**
     * Event to load a map
     * Load a new map
     *
     * @param controller Controller
     * @return command to load map
     */
    @Override
    public Command loadMapEvent(Controller controller) throws CommandException {
        return new LoadMapCommand(controller, this);
    }

    /**
     * Event to load a tour request
     *
     * @param controller Controller
     * @return command to load a tour
     */
    @Override
    public Command loadTourRequestEvent(Controller controller) throws CommandException {
        return new LoadTourRequestCommand(controller, this);
    }
}
