package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.*;
import com.insa.h4112.controller.command.other.ObtainRoadmap;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.view.ButtonBarView;
import com.insa.h4112.view.MainWindow;

/**
 * Optimized tour state
 *
 * @author Pierre-Yves GENEST
 */
public class OptimizedTourState implements State {
    /**
     * Managed map
     */
    protected Map map;

    /**
     * Managed tour request
     */
    protected TourRequest tourRequest;

    /**
     * Algorithm to use when optimizing tour
     */
    protected TourCompleteGraph tourCompleteGraph;

    /**
     * Optimized tour
     */
    protected Tour tour;

    /**
     * Constructor
     *
     * @param map         current map
     * @param tourRequest managed tour request
     */
    public OptimizedTourState(Map map, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph, Tour tour) {
        this.map = map;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
        this.tour = tour;
    }

    /**
     * Enter state
     * => set up state
     *
     * @param controller controller
     */
    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTourCompleteGraph(tourCompleteGraph);
        controller.setTour(tour);
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(true);
        mainWindow.setLoadTourRequestButtonEnable(true);
        mainWindow.setComputeTourButtonEnable(false);
        mainWindow.setGetPathDetailsButtonEnable(true);
        mainWindow.setAddCheckpointButtonEnable(true);
        mainWindow.setComputeTourButtonText(ButtonBarView.OPTIMIZE_BUTTON_TEXT_FINISHED);
    }

    /**
     * Leave state
     * => cleaning for next state
     *
     * @param controller controller
     */
    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTourCompleteGraph(null);
        controller.setTour(null);
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(false);
        mainWindow.setLoadTourRequestButtonEnable(false);
        mainWindow.setComputeTourButtonEnable(false);
        mainWindow.setGetPathDetailsButtonEnable(false);
        mainWindow.setAddCheckpointButtonEnable(false);
    }

    /**
     * Event to load a map
     *
     * @param controller Controller
     * @return command to load a map
     */
    @Override
    public Command loadMapEvent(Controller controller) throws CommandException {
        return new LoadMapCommand(controller, this);
    }

    /**
     * Event to load a tour request
     *
     * @param controller Controller
     * @return command to load a tour request
     */
    @Override
    public Command loadTourRequestEvent(Controller controller) throws CommandException {
        return new LoadTourRequestCommand(controller, this);
    }

    /**
     * Event to compute/optimize tour
     * recompute optimal tour
     *
     * @param controller Controller
     * @return command to optimize a tour
     */
    @Override
    public Command optimizeTourEvent(Controller controller) {
        return new ComputeOptimizedTourCommand(controller, this);
    }

    /**
     * Event to obtain the Roadmap.
     *
     * @param controller The controller.
     */
    @Override
    public void obtainRoadmapEvent(Controller controller) {
        new ObtainRoadmap(controller);
    }

    /**
     * Event to reorder checkpoints of tour
     *
     * @param controller Controller
     * @param from       Index of the checkpoint to move
     * @param to         Index where the moved checkpoint must go
     * @param tour       The associated Tour
     * @return command (or null)
     */
    @Override
    public Command moveCheckpointEvent(Controller controller, int from, int to, Tour tour) throws CommandException {
        return new ReorderCheckpointsCommand(controller, from, to, tour);
    }

    /**
     * Event to select a checkpoint
     *
     * @param controller        Controller
     * @param clickedCheckpoint clicked checkpoint
     * @return null
     */
    @Override
    public Command clickOnCheckpointEvent(Controller controller, Checkpoint clickedCheckpoint) {
        controller.changeState(new SelectedCheckpointState(map, tourRequest, tourCompleteGraph, tour, clickedCheckpoint, this));
        return null;
    }

    @Override
    public Command addDeliveryEvent(Controller controller) {
        return new AddingDeliveryEnterProcessCommand(controller, this);
    }
}
