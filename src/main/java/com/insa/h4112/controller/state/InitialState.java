package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.LoadMapCommand;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.view.MainWindow;

/**
 * Initial state of application.
 * It just can load the map.
 *
 * @author Pierre-Yves GENEST
 * @author Martin FRANCESCHI
 */
public class InitialState implements State {

    /**
     * Enter state
     * => set up state
     *
     * @param controller controller
     */
    @Override
    public void enter(Controller controller) {
        controller.setMap(null);

        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(true);
        mainWindow.setGetPathDetailsButtonEnable(false);
        mainWindow.setLoadTourRequestButtonEnable(false);
        mainWindow.setComputeTourButtonEnable(false);
    }

    @Override
    public void leave(Controller controller) {
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(false);
    }

    /**
     * Event to load a map.
     *
     * @param controller Controller
     * @return The command created, initialized but not executed.
     * @throws CommandException If the method fails: file cannot be opened, XML is not valid.
     */
    @Override
    public Command loadMapEvent(Controller controller) throws CommandException {
        return new LoadMapCommand(controller, this);
    }

}
