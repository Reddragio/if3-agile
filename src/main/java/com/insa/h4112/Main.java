package com.insa.h4112;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.view.MainWindow;

/**
 * Main class of application
 *
 * @author Pierre-Yves GENEST
 */
public class Main {

    /**
     * Main method
     *
     * @param args parameters
     */
    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow();
        Controller controller = new Controller();
        controller.setMainWindow(mainWindow);
        mainWindow.setController(controller);
        controller.setInitialState();
    }
}
