package com.insa.h4112.view;

import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;

/**
 * View that can show model objects
 *
 * @author Pierre-Yves GENEST
 */
public interface ModelView {

    /**
     * Show application map
     *
     * @param map map to show
     */
    void setMap(Map map);

    /**
     * Show tour request
     *
     * @param tourRequest tour request to display
     */
    void setTourRequest(TourRequest tourRequest);

    /**
     * Show tour
     *
     * @param tour tour to display
     */
    void setTour(Tour tour);
}
