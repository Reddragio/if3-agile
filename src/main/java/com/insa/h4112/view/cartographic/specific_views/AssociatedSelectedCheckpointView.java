package com.insa.h4112.view.cartographic.specific_views;

import com.insa.h4112.view.cartographic.CheckpointCartographicView;
import com.insa.h4112.view.util.Colors;

/**
 * To select an associated checkpoint (other member of delivery, warehouse...)
 *
 * @author Pierre-Yves GENEST
 */
public class AssociatedSelectedCheckpointView extends HighlightedCheckpointView {

    /**
     * Constructor
     *
     * @param associatedSelectedCheckpoint associated checkpoint
     */
    public AssociatedSelectedCheckpointView(CheckpointCartographicView associatedSelectedCheckpoint) {
        super(associatedSelectedCheckpoint, Colors.CHECKPOINT_ASSOCIATED_SELECTED_COLOR);
    }
}
