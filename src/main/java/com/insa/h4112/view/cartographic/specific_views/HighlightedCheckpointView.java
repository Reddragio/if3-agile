package com.insa.h4112.view.cartographic.specific_views;

import com.insa.h4112.view.cartographic.CheckpointCartographicView;
import com.insa.h4112.view.util.ShapeViewFactory;

import java.awt.*;

/**
 * Specific view to highlight a checkpoint
 *
 * @author Pierre-Yves GENEST
 */
public abstract class HighlightedCheckpointView implements SpecificView {
    /**
     * Highlighted checkpoint
     */
    private CheckpointCartographicView hightlightedCheckpoint;

    /**
     * Shape around checkpoint (to highlight)
     */
    private Shape highlightedShape;

    /**
     * Shape color
     */
    private Color shapeColor;

    /**
     * Constructor
     *
     * @param hightlightedCheckpoint checkpoint to highlight
     * @param shapeColor             color of shape
     */
    public HighlightedCheckpointView(CheckpointCartographicView hightlightedCheckpoint, Color shapeColor) {
        this.hightlightedCheckpoint = hightlightedCheckpoint;
        this.shapeColor = shapeColor;

        Rectangle bounds = hightlightedCheckpoint.getBounds();
        // Create shape
        switch (hightlightedCheckpoint.getType()) {
            case PICK_UP:
                // Draw a circle
                highlightedShape = ShapeViewFactory.createPickUpShape(bounds.x, bounds.y, 0, 0);
                break;
            case DELIVER:
                // Draw a rectangle
                highlightedShape = ShapeViewFactory.createDeliverShape(bounds.x, bounds.y, 0, 0);
                break;
            case WAREHOUSE:
                // Draw a triangle
                highlightedShape = ShapeViewFactory.createWarehouseShape(bounds.x, bounds.y, 0, 0);
                break;
            default:
                break;
        }
    }

    /**
     * To paint a component on screen
     *
     * @param graphics2D graphics element
     * @param unit       default size to paint an object
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        Rectangle bounds = hightlightedCheckpoint.getBounds();
        int x = bounds.x - bounds.width / 2;
        int y = bounds.y - bounds.height / 2;
        int w = bounds.width * 2;
        int h = bounds.height * 2;

        switch (hightlightedCheckpoint.getType()) {
            case PICK_UP:
                // Draw a circle
                ShapeViewFactory.updatePickUpShape(highlightedShape, x, y, w, h);
                break;
            case DELIVER:
                // Draw a rectangle
                ShapeViewFactory.updateDeliverShape(highlightedShape, x, y, w, h);
                break;
            case WAREHOUSE:
                // Draw a triangle
                ShapeViewFactory.updateWarehouseShape(highlightedShape, x, y, w, h);
                break;
            default:
                break;
        }

        graphics2D.setColor(shapeColor);
        graphics2D.fill(highlightedShape);
        hightlightedCheckpoint.paint(graphics2D, unit);
    }
}
