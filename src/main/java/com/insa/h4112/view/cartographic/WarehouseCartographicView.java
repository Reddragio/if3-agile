package com.insa.h4112.view.cartographic;

import com.insa.h4112.model.domain.CheckpointType;
import com.insa.h4112.model.domain.Warehouse;
import com.insa.h4112.view.util.Colors;

import java.awt.*;

public class WarehouseCartographicView implements View {

    /**
     * Warehouse that is displayed
     */
    private Warehouse warehouse;

    /**
     * Checkpoint representing warehouse
     */
    private CheckpointCartographicView warehouseCheckpointCartographicView;

    /**
     * Color of warehouse
     */
    private Color warehouseColor;

    /**
     * Constructor
     *
     * @param warehouse warehouse to display
     * @param map       mapView (to get intersectionView)
     * @param manager   manage checkpoint views
     */
    public WarehouseCartographicView(Warehouse warehouse, MapCartographicView map, CheckpointCartographicViewFactory manager) {
        this.warehouse = warehouse;
        warehouseColor = Colors.generateColor(warehouse.getSeed());

        // Create checkpoint
        warehouseCheckpointCartographicView = manager.getOrCreate(warehouse.getStart(), CheckpointType.WAREHOUSE, warehouseColor, map);
    }

    /**
     * To display warehouse
     *
     * @param graphics2D graphics element
     * @param unit       default unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        // Draw checkpoint
        warehouseCheckpointCartographicView.paint(graphics2D, unit);
    }
}
