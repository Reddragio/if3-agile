package com.insa.h4112.view.cartographic;

import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Section;
import com.insa.h4112.util.CoordinatesConversion;
import com.insa.h4112.view.util.ShapeViewFactory;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * View class responsible of displaying a {@link Section} of a {@link Map}
 *
 * @author Hugo Reymond
 * @author Paul Goux
 * @author Thomas Zhou
 * @author Pierre-Yves GENEST
 */
public class SectionCartographicView implements View {
    /**
     * Reference to model
     */
    private Section section;

    /**
     * Origin of section
     */
    private Point origin;

    /**
     * Destination of section
     */
    private Point destination;

    /**
     * Default {@link Color} of the intersection (when not on the Tour).
     */
    private static final Color DEFAULT_COLOR = Color.BLACK;

    /**
     * Stroke
     */
    private static final BasicStroke DEFAULT_STROKE = new BasicStroke(3);

    /**
     * Size of section arrow (to indicate direction)
     */
    private double arrowSize = 20.0;

    /**
     * Line drawn to represent the section
     */
    private Shape sectionLine;

    /**
     * Constructor
     *
     * @param section section to show on mapview
     */
    public SectionCartographicView(Section section) {
        this.section = section;

        // Compute origin/destination
        Point.Double originDouble = CoordinatesConversion.latLngToXY(section.getOrigin().getLatitude(), section.getOrigin().getLongitude());
        Point.Double destinationDouble = CoordinatesConversion.latLngToXY(section.getDestination().getLatitude(), section.getDestination().getLongitude());

        origin = new Point((int) originDouble.x, (int) originDouble.y);
        destination = new Point((int) destinationDouble.x, (int) destinationDouble.y);

        sectionLine = ShapeViewFactory.createSectionShape(originDouble.x, originDouble.y, destinationDouble.x, destinationDouble.y);
    }

    /**
     * Draw the section on the map
     *
     * @param graphics2D item to draw on
     * @param color      section color
     * @param stroke     of section
     */
    public void paint(Graphics2D graphics2D, Color color, BasicStroke stroke, boolean showArrow, double unit) {
        graphics2D.setColor(color);
        graphics2D.setStroke(stroke);
        graphics2D.draw(sectionLine);

        if (showArrow) {
            arrowSize = 3 * unit;
            Point2D.Double middle = new Point2D.Double((origin.x + destination.x) / 2.0, (origin.y + destination.y) / 2.0);
            Point2D.Double tangent = new Point2D.Double((destination.x - origin.x), (destination.y - origin.y));
            double norm = Math.sqrt(tangent.x * tangent.x + tangent.y * tangent.y);
            tangent.x *= 1.0 / norm * arrowSize;
            tangent.y *= 1.0 / norm * arrowSize;
            Point2D.Double perpendicular = new Point2D.Double(-tangent.y, tangent.x);
            Point2D.Double firstArrowCorner = new Point2D.Double(middle.x + perpendicular.x - tangent.x, middle.y + perpendicular.y - tangent.y);
            Point2D.Double secondArrowCorner = new Point2D.Double(middle.x - perpendicular.x - tangent.x, middle.y - perpendicular.y - tangent.y);
            //We draw the arrow:
            graphics2D.drawLine((int) middle.x, (int) middle.y, (int) firstArrowCorner.x, (int) firstArrowCorner.y);
            graphics2D.drawLine((int) middle.x, (int) middle.y, (int) secondArrowCorner.x, (int) secondArrowCorner.y);
        }
    }

    /**
     * Display Section on screen
     *
     * @param graphics2D graphics element
     * @param unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        paint(graphics2D, DEFAULT_COLOR, DEFAULT_STROKE, false, unit);
    }

    /**
     * Get section displayed byt this view
     *
     * @return section
     */
    public Section getSection() {
        return section;
    }
}
