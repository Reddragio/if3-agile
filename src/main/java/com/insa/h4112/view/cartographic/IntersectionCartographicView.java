package com.insa.h4112.view.cartographic;

import com.insa.h4112.controller.Events;
import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.util.CoordinatesConversion;
import com.insa.h4112.view.util.Colors;
import com.insa.h4112.view.util.ShapeViewFactory;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * View class responsible of displaying a {@link Intersection} of a {@link Map}
 *
 * @author Hugo Reymond
 * @author Paul Goux
 * @author Pierre-Yves GENEST
 */
public class IntersectionCartographicView implements View {

    /**
     * {@link Intersection} related to this view
     */
    private Intersection intersection;

    /**
     * Position of this intersection view (essentially circle center)
     */
    private Point.Double intersectionCenter;

    /**
     * Used to emit events (especially intersection selection)
     */
    private PropertyChangeSupport propertyChangeSupport;

    /**
     * Circle drawn to represent the intersection
     */
    private Shape intersectionCircle;

    /**
     * intersection size
     */
    private static final double INTERSECTION_SIZE = 14.;

    /**
     * Color of intersection
     */
    private static final Color COLOR = Colors.TRANSPARENT;

    /**
     * Constructor
     *
     * @param intersection intersection model to display
     */
    public IntersectionCartographicView(Intersection intersection) {
        this.intersection = intersection;
        intersectionCenter = CoordinatesConversion.latLngToXY(intersection.getLatitude(), intersection.getLongitude());
        intersectionCircle = ShapeViewFactory.createIntersectionShape(intersectionCenter.x - INTERSECTION_SIZE / 2, intersectionCenter.y - INTERSECTION_SIZE / 2, INTERSECTION_SIZE, INTERSECTION_SIZE);

        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    /**
     * Draw the intersection on the map
     *
     * @param graphics2D graphics object
     * @param unit       unit to use when zooming
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        graphics2D.setColor(COLOR);
        graphics2D.fill(intersectionCircle);
    }

    /**
     * To get bounds around intersection
     *
     * @return bounds around intersection
     */
    public Rectangle getBounds() {
        return intersectionCircle.getBounds();
    }

    /**
     * To get center of intersection
     *
     * @return intersection center
     */
    public Point.Double getCenter() {
        return intersectionCenter;
    }


    public void click() {
        propertyChangeSupport.firePropertyChange(Events.SELECT_INTERSECTION_EVENT, null, intersection);
    }

    /**
     * Add a listener to listen events
     *
     * @param listener listener that listens event
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
}
