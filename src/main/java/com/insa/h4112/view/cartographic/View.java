package com.insa.h4112.view.cartographic;

import java.awt.*;

/**
 * Minimum methods to show a component on screen
 *
 * @author Pierre-Yves GENEST
 */
public interface View {

    /**
     * To paint a component on screen
     *
     * @param graphics2D graphics element
     * @param unit       default size to paint an object
     */
    void paint(Graphics2D graphics2D, double unit);
}
