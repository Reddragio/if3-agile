package com.insa.h4112.view.cartographic;

import com.insa.h4112.controller.Events;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.CheckpointType;
import com.insa.h4112.view.util.ShapeViewFactory;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Class to display a checkpoint on cartographic view
 *
 * @author Pierre-Yves GENEST
 */
public class CheckpointCartographicView implements View {

    /**
     * Checkpoint shown by this view
     */
    private Checkpoint checkpoint;

    /**
     * Checkpoint type
     */
    private CheckpointType type;

    /**
     * Map cartographic view
     */
    private MapCartographicView map;

    /**
     * Shape that represents this checkpoint
     */
    private Shape checkpointShape;

    /**
     * Color of checkpoint (color of delivery)
     */
    private Color checkpointColor;

    /**
     * Used to emit events (especially checkpoint selection)
     */
    private PropertyChangeSupport propertyChangeSupport;

    /**
     * Shape ratio with reference unit
     */
    private static final int SHAPE_FACTOR = 6;

    /**
     * Constructor
     *
     * @param checkpoint checkpoint to show
     * @param type       type of checkpoint (warehouse, delivery, pick-up)
     * @param color      color of checkpoint
     * @param map        map view (to get intersection)
     */
    public CheckpointCartographicView(Checkpoint checkpoint, CheckpointType type, Color color, MapCartographicView map) {
        this.checkpoint = checkpoint;
        this.checkpointColor = color;
        this.type = type;
        this.map = map;

        propertyChangeSupport = new PropertyChangeSupport(this);

        Point.Double intersectionCenter = map.findIntersectionView(checkpoint.getIntersection()).getCenter();
        // Create shape
        switch (type) {
            case PICK_UP:
                // Draw a circle
                checkpointShape = ShapeViewFactory.createPickUpShape(intersectionCenter.x, intersectionCenter.y, 0, 0);
                break;
            case DELIVER:
                // Draw a rectangle
                checkpointShape = ShapeViewFactory.createDeliverShape(intersectionCenter.x, intersectionCenter.y, 0, 0);
                break;
            case WAREHOUSE:
                // Draw a triangle
                checkpointShape = ShapeViewFactory.createWarehouseShape(intersectionCenter.x, intersectionCenter.y, 0, 0);
                break;
            default:
                break;
        }
    }

    /**
     * To get bounds around checkpoint
     *
     * @return bounds around checkpoint
     */
    public Rectangle getBounds() {
        return checkpointShape.getBounds();
    }

    /**
     * Getter
     *
     * @return checkpoint type
     */
    public CheckpointType getType() {
        return type;
    }

    /**
     * To paint this checkpoint
     *
     * @param graphics2D graphics element
     * @param unit       default size to draw a view
     */
    public void paint(Graphics2D graphics2D, double unit) {
        double size = SHAPE_FACTOR * unit;

        Point.Double intersectionCenter = map.findIntersectionView(checkpoint.getIntersection()).getCenter();
        switch (type) {
            case PICK_UP:
                // Draw a circle
                ShapeViewFactory.updatePickUpShape(checkpointShape, intersectionCenter.x - size / 2, intersectionCenter.y - size / 2, size, size);
                break;
            case DELIVER:
                // Draw a rectangle
                ShapeViewFactory.updateDeliverShape(checkpointShape, intersectionCenter.x - size / 2, intersectionCenter.y - size / 2, size, size);
                break;
            case WAREHOUSE:
                // Draw a triangle
                ShapeViewFactory.updateWarehouseShape(checkpointShape, intersectionCenter.x - size / 2, intersectionCenter.y - size / 2, size, size);
                break;
            default:
                break;
        }

        graphics2D.setColor(checkpointColor);
        graphics2D.fill(checkpointShape);
    }

    /**
     * Click on coordinates
     *
     * @param coordinates click coordinates
     */
    public void click(Point.Double coordinates) {
        if (checkpointShape.contains(coordinates)) {
            propertyChangeSupport.firePropertyChange(Events.SELECT_CHECKPOINT_EVENT, null, checkpoint);
        }
    }

    /**
     * Add a listener to listen events
     *
     * @param listener listener that listens event
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove a listener from listening events
     *
     * @param listener listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
}
