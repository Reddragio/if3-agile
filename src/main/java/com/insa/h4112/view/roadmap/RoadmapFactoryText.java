package com.insa.h4112.view.roadmap;

import com.insa.h4112.model.domain.*;
import com.insa.h4112.view.util.TimeFormatter;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

/**
 * Implements a Roadmap Factory as raw text.
 *
 * @author Martin FRANCESCHI
 * @author Jacques CHARNAY
 */
public class RoadmapFactoryText extends AbstractRoadmapFactory {

    /**
     * The string builder
     */
    private StringBuilder builder = new StringBuilder();

    public RoadmapFactoryText() {
        super();
    }

    @Override
    public String getRoadmap(Tour theTour) {
        builder.setLength(0);
        tourRequest = theTour.getTourRequest();

        if (theTour.getCheckpoints().size() < 2) {
            printNoPathMessage();
        } else {
            printTitle("Vue d'ensemble");
            printMetadata(theTour);

            for (int i = 0; i < theTour.getAllPaths().size(); i++) {
                Path currentPath = theTour.getAllPaths().get(i);
                printPathMetadata(currentPath);
                printPathDetails(currentPath);
            }
        }

        return builder.toString();
    }

    @Override
    protected void printTitle(String text) {
        builder.append(String.format(
                "# ---- %s ----- # %s",
                text,
                System.lineSeparator()
        ));
    }

    @Override
    protected void printMetadata(Tour theTour) {
        LocalTime beginningTime = theTour.getFirstPath().getOrigin().getPassageTime();
        LocalTime endingTime = theTour.getLastPath().getDestination().getPassageTime();
        endingTime = endingTime.plus(theTour.getLastPath().getDestination().getDuration());

        builder.append(String.format(
                "Tournée de livraison du %s. %s",
                TimeFormatter.getShort(new Date()),
                System.lineSeparator()));

        builder.append(String.format(
                "Début : %s. Fin : %s. Durée totale : %s. %s",
                TimeFormatter.getShort(beginningTime),
                TimeFormatter.getShort(endingTime),
                TimeFormatter.getLiteral(beginningTime, endingTime),
                System.lineSeparator()));

        builder.append(String.format(
                "Emplacement de l'entrepôt : %s. %s",
                getIntersectionCoordinates(theTour.getTourRequest().getWarehouse().getStart().getIntersection()),
                System.lineSeparator().repeat(2)
        ));
    }

    @Override
    protected void printPathDetails(Path path) {
        if (!path.getSteps().isEmpty()) {
            // Declarations
            final String LINE_TO_FORMAT = "- %sSuivre %s durant %d mètres.%s"; // String to output for each expressed section.
            List<Section> sectionList = path.getSteps(); // All the sections of this path.
            Section currentSection = sectionList.get(0); // Section currently being studied.
            Section lastSection = currentSection; // Previously studied section.
            String currentStreetName = getSectionName(currentSection); // The name of the currently studied street.
            double currentStreetLength = currentSection.getLength(); // The currently known length of the currently studied street.
            PreciseDirection changeOfDirection = null; // Indicates if there has been a direction change since the previous section

            // Loop on every section of the path (except for the first one, already studied).
            for (int i = 1; i < sectionList.size(); i++) {
                currentSection = sectionList.get(i);

                if (getSectionName(currentSection).equals(currentStreetName)) {
                    // We are in the same street: just increment the street length.
                    currentStreetLength += currentSection.getLength();

                } else {
                    // We have changed street.
                    // We print the next street to follow, as well as the change of direction (if not null of course).
                    builder.append(String.format(
                            LINE_TO_FORMAT,
                            (changeOfDirection != null) ? directionToString(changeOfDirection) : "",
                            currentStreetName,
                            Math.round(currentStreetLength),
                            System.lineSeparator()));

                    // Finally we consider the new information about the new street.
                    changeOfDirection = lastSection.getPreciseDirectionToNextSection(currentSection);
                    currentStreetLength = currentSection.getLength();
                    currentStreetName = getSectionName(currentSection);
                }
                lastSection = currentSection;
            }

            // With this implementation we have to do an extra output for the last known street.
            builder.append(String.format(
                    LINE_TO_FORMAT,
                    (changeOfDirection != null) ? directionToString(changeOfDirection) : "",
                    currentStreetName,
                    Math.round(currentStreetLength),
                    System.lineSeparator()));

        }

        // Print the action to do at the end of the path.
        CheckpointType destinationType = tourRequest.getCheckpointType(path.getDestination());
        builder.append("     ");
        switch (destinationType) {
            case WAREHOUSE:
                builder.append("Arrivée à l'entrepôt.");
                break;
            case DELIVER:
                builder.append("Livrer le colis");
                break;
            case PICK_UP:
                builder.append("Récupérer le colis");
                break;
        }

        if (destinationType != CheckpointType.WAREHOUSE)
            builder.append(String.format(
                    " (durée : %s).",
                    TimeFormatter.getLiteral(path.getDestination().getDuration())
            ));

        // Make some space on the output.
        builder.append(System.lineSeparator().repeat(2));
    }

    @Override
    protected void printPathMetadata(Path path) {
        if (!path.getSteps().isEmpty()) {
            Checkpoint origin = path.getOrigin();
            Checkpoint destination = path.getDestination();
            CheckpointType originType = tourRequest.getCheckpointType(origin);
            CheckpointType destinationType = tourRequest.getCheckpointType(destination);
            String titleText = String.format(
                    "Trajet de %s à %s",
                    (originType == CheckpointType.WAREHOUSE) ?
                            "l'entrepôt" :
                            getSectionName(path.getSteps().get(0)),
                    (destinationType == CheckpointType.WAREHOUSE) ?
                            "l'entrepôt" :
                            getSectionName(path.getSteps().get(path.getSteps().size() - 1))
            );
            printTitle(titleText);

            builder.append(String.format(
                    "     Prévisionnel : départ à %s, arrivée à %s, temps de trajet de %s. %s",
                    TimeFormatter.getShort(origin.getPassageTime().plus(origin.getDuration())),
                    TimeFormatter.getShort(destination.getPassageTime()),
                    TimeFormatter.getLiteral(path.getPathDuration()),
                    System.lineSeparator()
            ));
        }
    }

    @Override
    protected void printNoPathMessage() {
        builder.append("Nous avons une excellente nouvelle : il n'y a aucun colis à livrer cette fois-ci !");
        builder.append(System.lineSeparator());
        builder.append(String.join(System.lineSeparator(),
                ".,__,.........,__,.....╭¬¬¬¬¬━━╮",
                "`•.,¸,.•*¯`•.,¸,.•*|:¬¬¬¬¬¬::::|:^--------^ ",
                "`•.,¸,.•*¯`•.,¸,.•*|:¬¬¬¬¬¬::::||｡◕‿‿◕｡| ",
                "-........--\"\"-.......--\"╰O━━━━O╯╰--O-O--╯"));
    }
}
