package com.insa.h4112.view.roadmap;

import com.insa.h4112.model.domain.Tour;

/**
 * Builds a Roadmap.
 * For use: initialize an instance and call getRoadmap.
 *
 * @author Martin FRANCESCHI
 */
public interface RoadmapFactory {

    /**
     * Converts the Tour to its String representation. The result is a long String!
     *
     * @param theTour The computed and completed Tour.
     * @return The final String instance describing the tour.
     */
    String getRoadmap(Tour theTour);
}
