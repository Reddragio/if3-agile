package com.insa.h4112.view.legend;

import com.insa.h4112.view.util.ShapeViewFactory;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Shape element linked with its description for the map legend
 *
 * @author Thomas Zhou
 */
public class LegendShapeDescription {

    /**
     * Shapes in the cartographic view
     */
    private List<Shape> shapes;

    /**
     * Meanings of the shape
     */
    private List<String> descriptions;

    /**
     * Constructor
     */
    public LegendShapeDescription() {
        shapes = new ArrayList<>();
        descriptions = new ArrayList<>();

        shapes.add(ShapeViewFactory.createWarehouseShape(0, 0, 20, 20));
        descriptions.add("Entrepôt");
        shapes.add(ShapeViewFactory.createPickUpShape(0, 0, 20, 20));
        descriptions.add("Point de récupération");
        shapes.add(ShapeViewFactory.createDeliverShape(0, 0, 20, 20));
        descriptions.add("Point de livraison");
    }

    /**
     * Get value of a cell
     *
     * @param columnIndex index of column
     * @return value in this cell
     */
    public Object getValueAt(int columnIndex) {
        int index = columnIndex / 2;

        // Display data as shape and description horizontally
        if (columnIndex % 2 == 0) {
            return shapes.get(index);
        }
        return descriptions.get(index);
    }
}
