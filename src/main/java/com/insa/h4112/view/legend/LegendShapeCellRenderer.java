package com.insa.h4112.view.legend;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Cell Renderer for the JTable in {@link LegendView}
 *
 * @author Thomas Zhou
 */
public class LegendShapeCellRenderer extends DefaultTableCellRenderer {

    /**
     * Shape to paint in the cell
     */
    Shape cellShape;

    /**
     * Render table cell
     *
     * @param table      table to render
     * @param value      value inside cell : MUST be a shape
     * @param isSelected if cell is selected
     * @param hasFocus   if celle has focus
     * @param row        row of cell
     * @param column     column of cell
     * @return cell component
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        cell.setHorizontalAlignment(SwingConstants.CENTER);
        cell.setVerticalAlignment(SwingConstants.CENTER);
        cell.setBackground(Color.lightGray);

        // Check if the cell is a shape(ie. Ellipse, Rectangle, ...)
        if (value instanceof Shape) {
            cellShape = (Shape) value;
            cell.setText("");
        } else {
            cellShape = null;
        }

        return cell;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (cellShape != null) {
            Rectangle bounds = cellShape.getBounds();
            int shapeWidth = bounds.width;
            int shapeHeight = bounds.height;
            Graphics2D g2d = (Graphics2D) g;
            int xOffset = (getWidth() - shapeWidth) / 2;
            int yOffset = (getHeight() - shapeHeight) / 2;
            g2d.translate(xOffset, yOffset);

            if (cellShape instanceof Line2D) {
                //Draw the section line shape
                g2d.setStroke(new BasicStroke(2));
                g2d.draw(cellShape);
            } else {
                g2d.fill(cellShape);
            }
        }
    }
}
