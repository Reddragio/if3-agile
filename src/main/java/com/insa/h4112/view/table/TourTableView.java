package com.insa.h4112.view.table;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.Events;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.CheckpointType;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.Warehouse;
import com.insa.h4112.view.util.Colors;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * View representing a tour (in table view)
 *
 * @author Pierre-Yves GENEST
 */
public class TourTableView extends ModelTableView implements Reorderable, PropertyChangeListener {

    /**
     * Displayed tour
     */
    private Tour tour;

    /**
     * Controller
     */
    public Controller controller;

    /**
     * Columns name
     */
    private static final String[] columnsName = {"Couleur", "Type", "Horaire", "Durée", "Intervalle"};

    /**
     * Constructor
     */
    public TourTableView(Controller controller) {
        super(controller);
        this.controller = controller;
        this.tour = null;
    }

    /**
     * Set tour
     *
     * @param tour tour
     */
    public void setTour(Tour tour) {
        this.tour = tour;
        this.checkpointTableViews.clear();

        if (tour != null) {
            Warehouse warehouse = tour.getTourRequest().getWarehouse();
            checkpointTableViews.add(checkpointManager.getOrCreate(warehouse.getStart(), CheckpointType.WAREHOUSE, Colors.generateColor(warehouse.getSeed())));
            for (Checkpoint checkpoint : tour.getCheckpoints()) {
                checkpointTableViews.add(checkpointManager.getOrCreate(checkpoint, tour.getTourRequest().getCheckpointType(checkpoint), Colors.generateColor(tour.getTourRequest().getAssociatedDelivery(checkpoint).getSeed())));
            }
            checkpointTableViews.add(checkpointManager.getOrCreate(warehouse.getEnd(), CheckpointType.WAREHOUSE, Colors.generateColor(warehouse.getSeed())));
        }
        fireTableDataChanged();
    }

    /**
     * Allow edit on the column 4 of the table to enable button click
     *
     * @param rowIndex    index of row
     * @param columnIndex index of column
     * @return true if the column needs editing
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 5;
    }

    /**
     * Delete the checkpoint and the matching checkpoint.
     *
     * @param rowIndex row to delete
     */
    public void deleteRow(int rowIndex) {
        //tour.removeCheckpoint(rowIndex);
        //TODO: eventually, create the future associated command
        fireTableDataChanged();
    }

    /**
     * Reorder a row
     *
     * @param fromIndex start index of row
     * @param toIndex   target index of row
     */
    @Override
    public void reorder(int fromIndex, int toIndex) {
        controller.reorderTourEvent(fromIndex - 1, toIndex - 1);
    }

    /**
     * Receive property change event
     *
     * @param propertyChangeEvent property change event
     */
    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        switch (propertyChangeEvent.getPropertyName()) {
            //That's normal, it's like an OR
            case Events.REMOVE_DELIVERY_FROM_TOUR_EVENT:
            case Events.ADD_DELIVERY_FROM_TOUR_EVENT:
            case Events.REORDER_CHECKPOINT_EVENT:
                setTour(tour);
                break;
        }
    }

    /**
     * Get column name
     *
     * @param columnIndex column index
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return columnsName[columnIndex];
    }

    /**
     * Get total number of columns
     *
     * @return total number of columns
     */
    @Override
    public int getColumnCount() {
        return columnsName.length;
    }
}
