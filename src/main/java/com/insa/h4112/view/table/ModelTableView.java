package com.insa.h4112.view.table;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.Events;

import javax.swing.table.AbstractTableModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Base class to display a view inside a TableView
 *
 * @author Pierre-Yves GENEST
 */
public abstract class ModelTableView extends AbstractTableModel implements PropertyChangeListener {

    /**
     * Checkpoint (sorted with tour order)
     */
    protected List<CheckpointTableView> checkpointTableViews;

    /**
     * Manager to interact with checkpoints views
     */
    protected CheckpointTableViewFactory checkpointManager;

    /**
     * Columns name
     */
    private static final String[] columnsName = {"Couleur", "Type", "Horaire", "Durée"};

    /**
     * Constructor
     */
    public ModelTableView(Controller controller) {
        checkpointTableViews = new ArrayList<>();
        checkpointManager = new CheckpointTableViewFactory(controller);
    }

    /**
     * Get column name
     *
     * @param columnIndex column index
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return columnsName[columnIndex];
    }

    /**
     * Get total number of rows
     *
     * @return total number of rows
     */
    @Override
    public int getRowCount() {
        return checkpointTableViews.size();
    }

    /**
     * Get total number of columns
     *
     * @return total number of columns
     */
    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    /**
     * Get value of a cell
     *
     * @param rowIndex    index of row
     * @param columnIndex index of column
     * @return value in this cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return checkpointTableViews.get(rowIndex).getValueAt(columnIndex);
    }

    /**
     * Click at a specific row
     *
     * @param row row to click
     * @return checkpoint view
     */
    public void clickAt(int row) {
        checkpointTableViews.get(row).click();
    }

    /**
     * To get checkpoint view manager
     *
     * @return checkpoint manager
     */
    public CheckpointTableViewFactory getCheckpointManager() {
        return checkpointManager;
    }

    /**
     * To receive events
     *
     * @param evt event
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case Events.REPAINT_EVENT:
                fireTableDataChanged();
                break;
        }
    }
}
