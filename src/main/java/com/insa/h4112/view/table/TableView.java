package com.insa.h4112.view.table;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.view.CheckpointSelectionType;
import com.insa.h4112.view.ModelView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Main container of table view
 *
 * @author Pierre-Yves GENEST
 */
public class TableView extends JPanel implements ModelView {
    /**
     * Panel containing checkpoints
     */
    private JTable jTable;

    /**
     * Model to display a tour request or a tour
     */
    private ModelTableView model;

    /**
     * Use to render color in a cell
     */
    private static final ColorCellRenderer COLOR_CELL_RENDERER = new ColorCellRenderer();

    /**
     * The controller to manage global application
     */
    private Controller controller = null;

    /**
     * Selected checkpoint
     */
    private CheckpointTableView selectedCheckpointView;

    /**
     * Checkpoint associated to selected checkpoint (other member of delivery, warehouse ...)
     */
    private CheckpointTableView associatedSelectedCheckpointView;

    /**
     * Constructor
     */
    public TableView() {
        setLayout(new GridLayout());
        setPreferredSize(new Dimension(650, 900));

        // Creating objects
        jTable = new JTable();
        model = new TourRequestTableView(controller);
        setModel(model);

        jTable.setDropMode(DropMode.INSERT_ROWS);
        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTable.setTransferHandler(new TableRowTransferHandler(jTable));

        // set up click
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                model.clickAt(jTable.rowAtPoint(e.getPoint()));
            }
        });

        // Setting up layout
        JScrollPane scrollPane = new JScrollPane(jTable);
        scrollPane.setViewportView(jTable);
        add(scrollPane);
    }

    /**
     * Display map (no effect)
     *
     * @param map map to show
     */
    @Override
    public void setMap(Map map) {
    }

    /**
     * Display tour request
     *
     * @param tourRequest tour request to display
     */
    @Override
    public void setTourRequest(TourRequest tourRequest) {
        TourRequestTableView tourRequestListView = new TourRequestTableView(controller);
        if (tourRequest != null) {
            tourRequestListView.setTourRequest(tourRequest);
        }
        setModel(tourRequestListView);
        jTable.setDragEnabled(false);
    }

    /**
     * Display tour
     *
     * @param tour tour to display
     */
    @Override
    public void setTour(Tour tour) {
        TourTableView tourListView = new TourTableView(controller);
        if (tour != null) {
            tour.getPropertyChangeSupport().addPropertyChangeListener(tourListView);
            tourListView.setTour(tour);
        }
        setModel(tourListView);
        jTable.setDragEnabled(true);
    }

    /**
     * Display specific model
     *
     * @param model model to display
     */
    private void setModel(ModelTableView model) {
        this.model = model;
        jTable.setModel(model);

        jTable.setDefaultRenderer(Object.class, COLOR_CELL_RENDERER);
        jTable.getColumnModel().getColumn(0).setMaxWidth(200);
    }

    /**
     * Set application controller
     *
     * @param controller the app's controller
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * Select a checkpoint
     *
     * @param selectedCheckpoint           checkpoint to select
     * @param associatedSelectedCheckpoint associated checkpoint to select
     */
    public void selectCheckpoints(Checkpoint selectedCheckpoint, Checkpoint associatedSelectedCheckpoint) {
        selectedCheckpointView = model.getCheckpointManager().find(selectedCheckpoint);
        if (selectedCheckpointView != null) {
            selectedCheckpointView.setSelected(CheckpointSelectionType.SELECTED);
        }

        associatedSelectedCheckpointView = model.getCheckpointManager().find(associatedSelectedCheckpoint);
        if (associatedSelectedCheckpointView != null) {
            associatedSelectedCheckpointView.setSelected(CheckpointSelectionType.ASSOCIATED);
        }
        repaint();
    }

    /**
     * Unselect checkpoint
     */
    public void unselectCheckpoints() {
        if (selectedCheckpointView != null) {
            selectedCheckpointView.setSelected(CheckpointSelectionType.NONE);
            selectedCheckpointView = null;
        }
        if (associatedSelectedCheckpointView != null) {
            associatedSelectedCheckpointView.setSelected(CheckpointSelectionType.NONE);
            associatedSelectedCheckpointView = null;
        }
        repaint();
    }

    /**
     * Highlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void highlightCheckpoint(Checkpoint checkpoint) {
        CheckpointTableView checkpointTableView = model.getCheckpointManager().find(checkpoint);
        if (checkpointTableView != null) {
            checkpointTableView.setSelected(CheckpointSelectionType.ASSOCIATED);
        }
    }

    /**
     * Unhighlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void unHighlightCheckpoint(Checkpoint checkpoint) {
        CheckpointTableView checkpointTableView = model.getCheckpointManager().find(checkpoint);
        if (checkpointTableView != null) {
            checkpointTableView.setSelected(CheckpointSelectionType.NONE);
        }
    }
}
