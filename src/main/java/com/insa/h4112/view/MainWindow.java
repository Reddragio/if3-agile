package com.insa.h4112.view;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.*;
import com.insa.h4112.util.log.Log;
import com.insa.h4112.view.cartographic.CartographicView;
import com.insa.h4112.view.legend.LegendView;
import com.insa.h4112.view.log.JTextPaneLogAppender;
import com.insa.h4112.view.table.ButtonBarTableView;
import com.insa.h4112.view.table.TableView;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.time.Duration;

/**
 * Main frame containing the user interface of the application
 *
 * @author Paul Goux
 * @author Thomas Zhou
 */
public class MainWindow extends JFrame {

    /**
     * Buttons at the top of the window
     */
    private ButtonBarView buttonBarView;

    /**
     * Manage global interaction with the application
     */
    private Controller controller = null;

    /**
     * Representation of Map, Tour Request, Tour in a cartographic view (left of window)
     */
    private CartographicView cartographicView;

    /**
     * Legend matching the checkpoint type meaning to its cartographic representation
     */
    private LegendView legendView;

    /**
     * Representation of Tour Request in a list view (right of window)
     */
    private TableView tableView;

    /**
     * Buttons on the right to change delivery List
     */
    private ButtonBarTableView buttonBarTableView;


    /**
     * Last directory where a file was selected
     */
    private File lastDirectory;

    private DurationSpinnerView durationSpinnerView;

    /**
     * Constructor
     */
    public MainWindow() {
        super("DELIVER'IF");
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        cartographicView = new CartographicView();
        tableView = new TableView();
        buttonBarView = new ButtonBarView();
        buttonBarTableView = new ButtonBarTableView();
        legendView = new LegendView();
        durationSpinnerView = new DurationSpinnerView();
        setVisibleSpinnerDuration(false);

        JPanel westPanel = new JPanel(new BorderLayout());
        westPanel.add(cartographicView, BorderLayout.CENTER);
        westPanel.add(legendView, BorderLayout.SOUTH);

        tableView.setBorder(BorderFactory.createEmptyBorder(5, 20, 20, 20));

        // Set up log
        JTextPane logInit = new JTextPane();
        JScrollPane scroll = new JScrollPane(logInit);
        scroll.setMinimumSize(new Dimension(0, 200));
        scroll.setPreferredSize(new Dimension(0, 200));
        JTextPaneLogAppender.setTextPane(logInit, scroll);

        // Borders
        tableView.setBorder(BorderFactory.createEmptyBorder(20, 20, 10, 20));
        scroll.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10, 20, 20, 20),
                BorderFactory.createTitledBorder("Logs")
        ));

        JPanel eastCenterPanel = new JPanel(new BorderLayout());
        eastCenterPanel.add(tableView, BorderLayout.CENTER);
        eastCenterPanel.add(buttonBarTableView, BorderLayout.NORTH);
        eastCenterPanel.add(durationSpinnerView, BorderLayout.SOUTH);

        JPanel eastPanel = new JPanel(new BorderLayout());
        eastPanel.add(eastCenterPanel, BorderLayout.CENTER);
        eastPanel.add(scroll, BorderLayout.SOUTH);

        mainPanel.add(westPanel, BorderLayout.CENTER);
        mainPanel.add(buttonBarView, BorderLayout.NORTH);
        mainPanel.add(eastPanel, BorderLayout.EAST);

        int WIFW = JComponent.WHEN_IN_FOCUSED_WINDOW;
        int CtrlKeyMask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx();
        mainPanel.getInputMap(WIFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, CtrlKeyMask), "CTRL_Z");
        mainPanel.getActionMap().put("CTRL_Z", new CtrlZAction());
        mainPanel.getInputMap(WIFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, CtrlKeyMask), "CTRL_Y");
        mainPanel.getActionMap().put("CTRL_Y", new CtrlYAction());
        mainPanel.getInputMap(WIFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, CtrlKeyMask + KeyEvent.SHIFT_DOWN_MASK), "CTRL_Y");
        mainPanel.getInputMap(WIFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, NORMAL), "ESCAPE");
        mainPanel.getActionMap().put("ESCAPE", new EscapeAction());
        mainPanel.getInputMap(WIFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, NORMAL), "DELETE");
        mainPanel.getActionMap().put("DELETE", new DeleteAction());
        mainPanel.getInputMap(WIFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, NORMAL), "DELETE");

        lastDirectory = FileSystemView.getFileSystemView().getHomeDirectory();

        this.setContentPane(mainPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setResizable(true);
        this.pack();
        this.setVisible(true);
    }

    /**
     * Ask the user to choose a XML file for loading the map or the tour request
     *
     * @return File
     */
    public File selectXMLFile() {
        JFileChooser jfc = new JFileChooser();
        jfc.setCurrentDirectory(lastDirectory);
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setMultiSelectionEnabled(false);
        jfc.setFileFilter(new FileNameExtensionFilter("Fichier XML", "xml"));

        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File chosenFile = jfc.getSelectedFile();
            lastDirectory = chosenFile.getParentFile();
            return chosenFile;
        }

        return null;
    }

    /**
     * Ask the user to choose any file, even if not existing.
     *
     * @return The chosen file, or null if no file was selected.
     */
    public File chooseAnyFile() {
        JFileChooser jfc = new JFileChooser();
        jfc.setCurrentDirectory(lastDirectory);
        int returnOfSaveDialog = jfc.showSaveDialog(this);
        if (returnOfSaveDialog != JFileChooser.APPROVE_OPTION) {
            return null;
        }
        File chosenFile = jfc.getSelectedFile();
        lastDirectory = chosenFile.getParentFile();
        return chosenFile;
    }

    /**
     * Display the map
     *
     * @param map map to be displayed
     */
    public void setMap(Map map) {
        cartographicView.setMap(map);
        tableView.setMap(map);
    }

    /**
     * To set tour
     * Display the tour
     *
     * @param tour new tour
     */
    public void setTour(Tour tour) {
        cartographicView.setTour(tour);
        tableView.setTour(tour);
        if (tour != null) {
            tour.getPropertyChangeSupport().addPropertyChangeListener(cartographicView);
        }
    }

    public void setTourRequest(TourRequest tourRequest) {
        cartographicView.setTourRequest(tourRequest);
        tableView.setTourRequest(tourRequest);
        if (tourRequest != null) {
            tourRequest.getPropertyChangeSupport().addPropertyChangeListener(cartographicView);
        }
    }

    private class CtrlZAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean res = controller.undoEvent();
            if (res) {
                Log.successUndo();
            }
        }
    }

    private class CtrlYAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean res = controller.redoEvent();
            if (res) {
                Log.successDo();
            }
        }
    }

    private class EscapeAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            controller.escapeKeyEvent();
        }
    }

    private class DeleteAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            controller.deleteKeyEvent();
        }
    }

    public void setLoadMapButtonEnable(boolean enable) {
        buttonBarView.setLoadMapButtonEnable(enable);
    }

    public void setLoadTourRequestButtonEnable(boolean enable) {
        buttonBarView.setLoadTourRequestButtonEnable(enable);
    }

    public void setComputeTourButtonEnable(boolean enable) {
        buttonBarView.setComputeTourButtonEnable(enable);
    }

    public void setGetPathDetailsButtonEnable(boolean enable) {
        buttonBarView.setObtainRoadmapButtonEnable(enable);
    }

    public void setDeleteCheckpointButtonEnable(boolean enable) {
        buttonBarTableView.setDeleteCheckpointButtonEnable(enable);
    }

    public void setAddCheckpointButtonEnable(boolean enable) {
        buttonBarTableView.setAddCheckpointButtonEnable(enable);
    }

    public void setComputeTourButtonText(String text) {
        buttonBarView.setComputeTourButtonText(text);
    }

    public void setLegendVisible(boolean visible) {
        legendView.setVisible(visible);
    }

    /**
     * Set application controller
     *
     * @param controller controller
     */
    public void setController(Controller controller) {
        this.controller = controller;
        buttonBarView.setController(controller);
        cartographicView.setController(controller);
        tableView.setController(controller);
        buttonBarTableView.setController(controller);
    }

    /**
     * Select checkpoints
     *
     * @param selectedCheckpoint           checkpoint to select
     * @param associatedSelectedCheckpoint associated checkpoint
     */
    public void selectCheckpoints(Checkpoint selectedCheckpoint, Checkpoint associatedSelectedCheckpoint) {
        tableView.selectCheckpoints(selectedCheckpoint, associatedSelectedCheckpoint);
        cartographicView.selectCheckpoints(selectedCheckpoint, associatedSelectedCheckpoint);
    }

    /**
     * Unselect checkpoints
     *
     * @param selectedCheckpoint           checkpoint to unselect
     * @param associatedSelectedCheckpoint associated checkpoint
     */
    public void unselectCheckpoints(Checkpoint selectedCheckpoint, Checkpoint associatedSelectedCheckpoint) {
        tableView.unselectCheckpoints();
        cartographicView.unselectCheckpoints(selectedCheckpoint, associatedSelectedCheckpoint);
    }

    public void setVisibleSpinnerDuration(boolean visible) {
        durationSpinnerView.setVisible(visible);
    }

    public void displayPickUpDuration() {
        durationSpinnerView.setBeforeSpinnerString("Temps de chargement : ");
        durationSpinnerView.setAfterSpinnerString("minutes");
        setVisibleSpinnerDuration(true);
    }

    public void displayDeliverDuration() {
        durationSpinnerView.setBeforeSpinnerString("Temps de livraison : ");
        durationSpinnerView.setAfterSpinnerString("minutes");
        setVisibleSpinnerDuration(true);
    }

    public void selectIntersection(Intersection intersection) {
        cartographicView.selectIntersection(intersection);
    }

    public void unselectIntersection(Intersection intersection) {
        cartographicView.unselectIntersection(intersection);
    }

    /**
     * Highlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void highlightCheckpoint(Checkpoint checkpoint) {
        cartographicView.highlightCheckpoint(checkpoint);
        tableView.highlightCheckpoint(checkpoint);
    }

    /**
     * Unhighlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void unHighlightCheckpoint(Checkpoint checkpoint) {
        cartographicView.unHighlightCheckpoint(checkpoint);
        tableView.unHighlightCheckpoint(checkpoint);
    }

    public Duration getDurationInSpinner() {
        return durationSpinnerView.getDuration();
    }

    public void resetDurationSpinner() {
        durationSpinnerView.resetSpinner();
    }
}


