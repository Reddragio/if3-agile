package com.insa.h4112.util;

/**
 * Procedure (lambda) that throws an exception
 * no parameter, no return value
 *
 * @author Pierre-Yves GENEST
 */
@FunctionalInterface
public interface Procedure<E extends Exception> {

    /**
     * Called to run procedure
     *
     * @throws E exception that this procedure throws
     */
    void run() throws E;
}
