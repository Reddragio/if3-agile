package com.insa.h4112.util;

import java.awt.*;

/**
 * Convert latitude-longitude -> x-y and vice et versa
 *
 * @author Pierre-Yves GENEST
 */
public class CoordinatesConversion {
    /**
     * Earth radius (in meters)
     */
    private static final int EARTH_RADIUS = 6371000;

    /**
     * Deleted constructor
     */
    private CoordinatesConversion() {
    }

    /**
     * Convert latitude/longitude to x y coordinates
     *
     * @return x y in meters
     */
    public static Point.Double latLngToXY(double latitude, double longitude) {
        double latitudeRad = Math.toRadians(latitude);
        double longitudeRad = Math.toRadians(longitude);

        double x = EARTH_RADIUS * Math.cos(latitudeRad) * Math.sin(longitudeRad);
        double y = EARTH_RADIUS * Math.cos(latitudeRad) * Math.cos(longitudeRad);

        return new Point.Double(x, y);
    }
}
