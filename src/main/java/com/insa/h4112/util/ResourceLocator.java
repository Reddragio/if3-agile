package com.insa.h4112.util;

/**
 * To find resources
 *
 * @author Pierre-Yves GENEST
 */
public class ResourceLocator {

    /**
     * Locate a resource
     *
     * @param resourceName resource name (for instance : MapSchema.xsd)
     * @return path to resource
     */
    public static String locateResource(String resourceName) {
        return ResourceLocator.class.getClassLoader().getResource(resourceName).toExternalForm();
    }

    /**
     * Deleted constructor
     */
    private ResourceLocator() {
    }
}
