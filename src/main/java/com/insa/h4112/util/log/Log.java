package com.insa.h4112.util.log;

import com.insa.h4112.controller.state.State;
import com.insa.h4112.view.util.TimeFormatter;

import java.io.File;

/**
 * To access logger
 * To create a new message, create a string and a static void method.
 *
 * @author Pierre-Yves GENEST
 * @author Martin FRANCESCHI
 */
public class Log {

    /* ----- THE STRINGS TO BE DISPLAYED ----- */

    /* ----- Errors from commands ----- */
    public final static String ERROR_COMMAND_XML_INVALID = "Le fichier XML est invalide";
    public final static String ERROR_COMMAND_NO_FILE_SELECTED = "Aucun fichier n'a été sélectionné";
    public final static String ERROR_COMMAND_XML_NOT_FOUND = "Le fichier XML n'a pas pu être ouvert";
    public final static String ERROR_COMMAND_INVALID_TOUR_REQUEST = "La demande de tournée est invalide: certains points de passages sont inaccessibles depuis l'entrepôt";

    /* ----- Success of any operation ----- */
    private final static String SUCCESS_UNDO = "L'annulation a réussi";
    private final static String SUCCESS_REDO = "Le rétablissement a réussi";
    private final static String SUCCESS_OPEN_FILE = "Le fichier \"%s\" a été ouvert avec succès";
    private final static String SUCCESS_OPTIMIZE_TOUR = "La tournée a été calculée en %s";
    private final static String SUCCESS_OPTIMIZE_TOUR_OPTIMAL = "Bonne nouvelle : la tournée calculée est optimale";
    private final static String SUCCESS_OPTIMIZE_TOUR_NON_OPTIMAL = "La tournée calculée n'est pas optimale";
    private final static String SUCCESS_ROADMAP_CLIPBOARD = "La feuille de route a été copiée dans le presse-papier";
    private final static String SUCCESS_ROADMAP_EXPORT = "La feuille de route a été exportée vers \"%s\" avec succès";
    private final static String SUCCESS_DELETE_DELIVERY = "La livraison a été supprimée avec succès";
    private final static String SUCCESS_ADD_DELIVERY = "La livraison a été ajoutée avec succès";

    /* ----- Specific process: add delivery events ----- */
    public final static String INFO_SELECT_PICKUP = "1) Veuillez saisir la durée de récupération du colis puis sélectionner le point de récupération";
    public final static String INFO_SELECT_BEFORE_PICKUP = "2) Veuillez sélectionner le point de passage précédant le point de récupération";
    public final static String INFO_SELECT_DELIVER = "3) Veuillez saisir la durée de livraison du colis puis sélectionner le point de livraison";
    public final static String INFO_SELECT_BEFORE_DELIVER = "4) Veuillez sélectionner le point de passage précédant le point de livraison";
    public final static String INFO_CANCEL_ADDING_DELIVER = "L'ajout de la livraison a été annulé";

    /* ----- Failure of an operation ----- */
    private final static String FAIL_ROADMAP_EXPORT = "L'exportation de la feuille de route vers \"%s\" a échoué ; raison invoquée : %s";

    /* ----- Other ----- */
    private final static String CONTROLLER_STATE = "Le contrôleur est maintenant dans l'état : ";
    private final static String START_COMPUTING_OPTIMIZATION = "Le calcul est en cours...";


    /* ----- THE METHODS TO CALL ----- */

    public static void controllerState(State state) {
        log.trace(CONTROLLER_STATE + state.name());
    }

    public static void errorCommand(String message) {
        log.error(message);
    }

    public static void successUndo() {
        log.trace(SUCCESS_UNDO);
    }

    public static void successDo() {
        log.trace(SUCCESS_REDO);
    }

    public static void successLoadTourRequest(File file) {
        log.succ(String.format(SUCCESS_OPEN_FILE, file.getName()));
    }

    public static void successLoadMap(File file) {
        log.succ(String.format(SUCCESS_OPEN_FILE, file.getName()));
    }

    public static void successOptimizeTour(boolean optimum, long durationInMillis) {
        log.succ(String.format(
                SUCCESS_OPTIMIZE_TOUR,
                TimeFormatter.getSecondsLiteral(durationInMillis)
        ));
        if (optimum) {
            log.succ(SUCCESS_OPTIMIZE_TOUR_OPTIMAL);
        } else {
            log.warn(SUCCESS_OPTIMIZE_TOUR_NON_OPTIMAL);
        }
    }

    public static void successRoadmapClipboard() {
        log.succ(SUCCESS_ROADMAP_CLIPBOARD);
    }

    public static void successRoadmapExport(File file) {
        log.succ(String.format(SUCCESS_ROADMAP_EXPORT, file.getName()));
    }

    public static void successDeleteDelivery() {
        log.succ(SUCCESS_DELETE_DELIVERY);
    }

    public static void successAddDelivery() {
        log.succ(SUCCESS_ADD_DELIVERY);
    }

    public static void failRoadmapExport(File file, String message) {
        log.error(String.format(FAIL_ROADMAP_EXPORT, file.getName(), message));
    }

    public static void beginOptimization() {
        log.info(START_COMPUTING_OPTIMIZATION);
    }

    /* ----- IMPLEMENTATION OF THE LOGGER ----- */

    /**
     * Deleted constructor.
     */
    private Log() {
    }

    /**
     * The unique logger
     */
    private static final ExtendedLogger log = ExtendedLogger.create();

    /**
     * Outputs debug message on the console.
     * FOR PRODUCTION ONLY
     *
     * @param output      message to display
     * @param sourceClass the class at the origin of the message
     */
    public static void debug(String output, Class sourceClass) {
        debug(String.format("{%s} :: %s", sourceClass.getSimpleName(), output));
    }

    /**
     * Outputs debug message on the console.
     * FOR PRODUCTION ONLY
     *
     * @param output message to display
     */
    public static void debug(String output) {
        log.debug(output);
    }

    /**
     * Outputs information message on the console.
     *
     * @param output message to display
     */
    public static void info(String output) {
        log.info(output);
    }
}
