package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.*;

import java.util.*;

/**
 * Algorithm class to generate the complete checkpoint graph
 * It also create a Map to easily know if a Checkpoint must be preceded
 * by an other Checkpoint (because they belong to the same Delivery
 * and one is the pick-up checkpoint and the other the deliver checkpoint)
 *
 * @author Jacques CHARNAY
 */
public class TourCompleteGraph {

    /**
     * Used map
     */
    private Map map;

    /**
     * Checkpoints to reach
     */
    private List<Checkpoint> checkpoints;

    /**
     * Distance for each delivery
     */
    private java.util.Map<BasicPair<Checkpoint, Checkpoint>, Double> distances;

    /**
     * Path of each delivery
     */
    private java.util.Map<BasicPair<Checkpoint, Checkpoint>, Path> paths;

    /**
     * Checkpoint precedence map
     */
    private java.util.Map<Checkpoint, Checkpoint> precededBy;

    /**
     * constructor
     *
     * @param map         map to use
     * @param tourRequest tour request to use (contain all delivery)
     */
    public TourCompleteGraph(Map map, TourRequest tourRequest) {
        this.map = map;
        checkpoints = new ArrayList<>();
        distances = new HashMap<>();
        paths = new HashMap<>();
        precededBy = new HashMap<>();
        generateCompleteGraph(tourRequest);
    }

    /**
     * return all checkpoints
     *
     * @return list of all checkpoints
     */
    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    /**
     * distance map of each delivery
     *
     * @return distance map
     */
    public java.util.Map<BasicPair<Checkpoint, Checkpoint>, Double> getDistances() {
        return distances;
    }

    /**
     * path map of each delivery
     *
     * @return path map
     */
    public java.util.Map<BasicPair<Checkpoint, Checkpoint>, Path> getPaths() {
        return paths;
    }

    /**
     * return checkpoint precedence map
     *
     * @return precedence map
     */
    public java.util.Map<Checkpoint, Checkpoint> getPrecededBy() {
        return precededBy;
    }

    /**
     * Gather all the checkpoints from the tourRequest given and generate
     * the complete graph connecting all checkpoints together thanks
     * to multiple Dijkstra executions
     *
     * @param tourRequest the tourRequest which you want the complete graph
     */
    private void generateCompleteGraph(TourRequest tourRequest) {
        Warehouse warehouse = tourRequest.getWarehouse();
        Checkpoint start = warehouse.getStart();
        Checkpoint end = warehouse.getEnd();
        checkpoints.add(start);
        checkpoints.add(end);
        precededBy.put(start, null);
        precededBy.put(end, start);

        List<Delivery> deliveries = tourRequest.getDeliveries();
        Checkpoint pickUpPoint, deliverPoint;
        for (Delivery delivery : deliveries) {
            pickUpPoint = delivery.getPickUp();
            checkpoints.add(pickUpPoint);
            deliverPoint = delivery.getDeliver();
            checkpoints.add(deliverPoint);

            precededBy.put(pickUpPoint, null);
            precededBy.put(deliverPoint, pickUpPoint);
        }

        Set<Intersection> checkpointGoals = new HashSet<>();
        for (Checkpoint checkpoint : checkpoints) {
            checkpointGoals.add(checkpoint.getIntersection());
        }

        DijkstraDistanceMap cptDistMap;
        Intersection otherInter;
        BasicPair<Checkpoint, Checkpoint> newPair;
        for (Checkpoint node : checkpoints) {
            //Dijsktra execution from the current Checkpoint:
            cptDistMap = new DijkstraDistanceMap(map, node.getIntersection(), checkpointGoals);
            for (Checkpoint otherNode : checkpoints) {
                //We save the distance to all others checkpoints:
                otherInter = otherNode.getIntersection();
                newPair = new BasicPair(node, otherNode);
                distances.put(newPair, cptDistMap.distanceToInter(otherInter));
                paths.put(newPair, new Path(node, otherNode, cptDistMap.getPathTo(otherInter)));
            }
        }
    }

    /**
     * return the length of the path of minimal length between two checkpoints
     *
     * @param cp1 the first checkpoint
     * @param cp2 the second checkpoint
     * @return the minimal length between cp1 and cp2
     */
    public Double getDistBTWCheckpoints(Checkpoint cp1, Checkpoint cp2) {
        return distances.get(new BasicPair(cp1, cp2));
    }

    /**
     * return the path of minimal length between two checkpoints
     *
     * @param cp1 the first checkpoint
     * @param cp2 the second checkpoint
     * @return the path of minimal length between cp1 and cp2
     */
    public Path getPathBTWCheckpoints(Checkpoint cp1, Checkpoint cp2) {
        return paths.get(new BasicPair(cp1, cp2));
    }

    /**
     * if the checkpoint is a deliver point, return the associated pick-up point.
     * else, if the checkpoint is the begin/end point or a pick-up point, return null
     *
     * @param cp the checkpoint that interests you
     * @return the associated pick-up point, or null if it doesn't exist
     */
    public Checkpoint getCheckpointPredecessor(Checkpoint cp) {
        return precededBy.get(cp);
    }

    /**
     * Verify that all checkpoints listed in the TourCompleteGraph are reachable
     * from the given warehouse
     *
     * @param warehouse the given warehouse
     * @return true, if all checkpoints are reachable, false otherwise
     */
    public boolean verifyAllCheckpointsReachableFromWarehouse(Warehouse warehouse) {
        Checkpoint start = warehouse.getStart();
        Checkpoint end = warehouse.getEnd();
        for (Checkpoint checkpoint : checkpoints) {
            if (distances.get(new BasicPair<>(start, checkpoint)) == Double.MAX_VALUE
                    || distances.get(new BasicPair<>(checkpoint, end)) == Double.MAX_VALUE) {
                return false;
            }
        }
        return true;
    }

    /**
     * Verify that a new checkpoint (not already present in the tourCompleteGraph) is reachable
     * from the given warehouse
     *
     * @param warehouse  the given warehouse
     * @param checkpoint the new checkpoint
     * @return true, if the checkpoint is reachable, false otherwise
     */
    public boolean verifyCheckpointReachableFromWarehouse(Warehouse warehouse, Checkpoint checkpoint) {
        Intersection start = warehouse.getStart().getIntersection();
        Intersection end = warehouse.getEnd().getIntersection();
        Intersection inter = checkpoint.getIntersection();

        Set<Intersection> goalCheckpoint = new HashSet<>();
        goalCheckpoint.add(inter);
        Set<Intersection> goalEnd = new HashSet<>();
        goalEnd.add(end);

        DijkstraDistanceMap dmapTo = new DijkstraDistanceMap(map, start, goalCheckpoint);
        DijkstraDistanceMap dmapFrom = new DijkstraDistanceMap(map, inter, goalEnd);
        return dmapTo.distanceToInter(inter) != Double.MAX_VALUE
                && dmapFrom.distanceToInter(end) != Double.MAX_VALUE;
    }

    /**
     * Add a delivery to the current model
     *
     * @param newDelivery delivery to add
     */
    public void addDelivery(Delivery newDelivery) {
        Checkpoint newPickUpPoint = newDelivery.getPickUp();
        Checkpoint newDeliverPoint = newDelivery.getDeliver();
        checkpoints.add(newPickUpPoint);
        checkpoints.add(newDeliverPoint);
        precededBy.put(newPickUpPoint, null);
        precededBy.put(newDeliverPoint, newPickUpPoint);

        Set<Intersection> checkpointGoals = new HashSet<>();
        for (Checkpoint checkpoint : checkpoints) {
            checkpointGoals.add(checkpoint.getIntersection());
        }
        Set<Intersection> newCheckpointGoals = new HashSet<>();
        newCheckpointGoals.add(newPickUpPoint.getIntersection());
        newCheckpointGoals.add(newDeliverPoint.getIntersection());

        DijkstraDistanceMap cptDistMap;
        Intersection otherInter;
        BasicPair<Checkpoint, Checkpoint> pair;
        for (Checkpoint node : checkpoints.subList(0, checkpoints.size() - 2)) {
            //We execute a Dijkstra from all previous checkpoints to discover the path/distance
            //to the new 2 checkpoints
            cptDistMap = new DijkstraDistanceMap(map, node.getIntersection(), newCheckpointGoals);
            for (Checkpoint otherNode : checkpoints.subList(checkpoints.size() - 2, checkpoints.size())) {
                otherInter = otherNode.getIntersection();
                pair = new BasicPair<>(node, otherNode);
                distances.put(pair, cptDistMap.distanceToInter(otherInter));
                paths.put(pair, new Path(node, otherNode, cptDistMap.getPathTo(otherInter)));
            }
        }
        for (Checkpoint node : checkpoints.subList(checkpoints.size() - 2, checkpoints.size())) {
            //We execute a Dijkstra from the 2 new checkpoints to discover the path/distance
            //to all checkpoints
            cptDistMap = new DijkstraDistanceMap(map, node.getIntersection(), checkpointGoals);
            for (Checkpoint otherNode : checkpoints) {
                otherInter = otherNode.getIntersection();
                pair = new BasicPair<>(node, otherNode);
                distances.put(pair, cptDistMap.distanceToInter(otherInter));
                paths.put(pair, new Path(node, otherNode, cptDistMap.getPathTo(otherInter)));
            }
        }
    }

    /**
     * delete delivery of the current model
     *
     * @param delivery delivery to delete
     */
    public void removeDelivery(Delivery delivery) {
        Checkpoint pickUpPoint = delivery.getPickUp();
        Checkpoint deliverPoint = delivery.getDeliver();
        checkpoints.remove(pickUpPoint);
        checkpoints.remove(deliverPoint);
        precededBy.remove(pickUpPoint);
        precededBy.remove(deliverPoint);

        BasicPair<Checkpoint, Checkpoint> newPair;
        for (Checkpoint node : checkpoints) {
            newPair = new BasicPair<>(node, pickUpPoint);
            distances.remove(newPair);
            paths.remove(newPair);
            newPair = new BasicPair<>(node, deliverPoint);
            distances.remove(newPair);
            paths.remove(newPair);

            newPair = new BasicPair<>(pickUpPoint, node);
            distances.remove(newPair);
            paths.remove(newPair);
            newPair = new BasicPair<>(deliverPoint, node);
            distances.remove(newPair);
            paths.remove(newPair);
        }
        newPair = new BasicPair<>(pickUpPoint, pickUpPoint);
        distances.remove(newPair);
        paths.remove(newPair);
        newPair = new BasicPair<>(pickUpPoint, deliverPoint);
        distances.remove(newPair);
        paths.remove(newPair);
        newPair = new BasicPair<>(deliverPoint, deliverPoint);
        distances.remove(newPair);
        paths.remove(newPair);
        newPair = new BasicPair<>(deliverPoint, pickUpPoint);
        distances.remove(newPair);
        paths.remove(newPair);

    }

}
