package com.insa.h4112.model.algorithm;

import java.util.Objects;

/**
 * Basic class to represent a Pair of any given Object
 *
 * @author Jacques CHARNAY
 */
public class BasicPair<A, B> {

    /**
     * First element, instance of A
     */
    protected A first;

    /**
     * Second element, instance of B
     */
    protected B second;

    /**
     * Constructeur
     *
     * @param first  first element
     * @param second second element
     */
    public BasicPair(A first, B second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Return first element of the pair
     *
     * @return first element of the pair
     */
    public A getFirst() {
        return first;
    }

    /**
     * Set first element of the pair
     *
     * @param first new first element
     */
    public void setFirst(A first) {
        this.first = first;
    }

    /**
     * Return second element of the pair
     *
     * @return second element of the pair
     */
    public B getSecond() {
        return second;
    }

    /**
     * Set second element of the pair
     *
     * @param second new second element
     */
    public void setSecond(B second) {
        this.second = second;
    }

    /**
     * Test equality
     *
     * @param o object to test with
     * @return true if o and this are equals, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicPair<?, ?> basicPair = (BasicPair<?, ?>) o;
        return Objects.equals(first, basicPair.first) &&
                Objects.equals(second, basicPair.second);
    }

    /**
     * return the hashCode
     *
     * @return int for the hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
