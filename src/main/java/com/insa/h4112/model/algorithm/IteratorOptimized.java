package com.insa.h4112.model.algorithm;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Iterator class to browse efficiently all unseen checkpoints during TSP algorithm.
 * It implements a simple mechanism to respect the constraints of precedence.
 * (The class is package-private)
 *
 * @author First Version from: Christine SOLNON - Modified by: Jacques CHARNAY
 */
class IteratorOptimized implements Iterator<Integer> {

    /**
     * Array that contains all the checkpoints that we will browse
     * thanks to the iterator
     */
    private double[][] candidates;

    /**
     * Index that allow us to iterate over the above array
     */
    private int indexCandidates;

    /**
     * Number of cells of the arrays really used
     * In fact, to stay simple, we build an array to receive as many elements as we could receive,
     * but sometimes we have finally less elements (because of constraints of precedence)
     */
    private int realSize;

    /**
     * Create an iterator to browse efficiently all unseen checkpoints,
     * that can be accessed with regard to the constraints of precedence
     * Our strategy is to go to the nearest checkpoints from currentNode.
     *
     * @param currentNode  The node where we are currently in research
     * @param unseen       The unseen checkpoints
     * @param seenBoolean  The already seen checkpoints (in the form of a boolean array, no information of order)
     * @param cost         A 2d array of the cost to travel between each couple of checkpoints
     * @param predecessors An array that indicates if a checkpoint must be preceded by an other checkpoint
     */
    public IteratorOptimized(Integer currentNode, List<Integer> unseen, boolean[] seenBoolean, double[][] cost, int[] predecessors) {
        this.candidates = new double[unseen.size()][2];
        indexCandidates = -1;
        realSize = 0;

        for (Integer candidate : unseen) {
            if (predecessors[candidate] == -1 || seenBoolean[predecessors[candidate]]) {
                candidates[realSize][0] = cost[currentNode][candidate];
                candidates[realSize][1] = (double) candidate;
                ++realSize;
            }
        }

        //We sort the array in ascending order, in order to browse first the nearest node from current node
        Arrays.sort(candidates, 0, realSize, Comparator.comparingDouble(a -> a[0]));
    }

    @Override
    public boolean hasNext() {
        return indexCandidates < realSize - 1;
    }

    @Override
    public Integer next() {
        return (int) candidates[++indexCandidates][1];
    }

    @Override
    public void remove() {
    }

}
