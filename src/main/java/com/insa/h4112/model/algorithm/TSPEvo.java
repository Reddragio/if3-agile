package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.TourRequest;

import java.util.Iterator;
import java.util.List;

/**
 * Class to resolve efficiently the TSP problem in order to build the most optimized Tour possible
 *
 * @author First Version from: Christine SOLNON - Modified by: Jacques CHARNAY
 */
public class TSPEvo extends TemplateTSPEvo {

    /**
     * the Constructor, used to initialize all the data necessary for the execution of the TSP algorithm.
     * Mainly, it converts work objects into most efficient data structures, in this case simple arrays of integers
     *
     * @param tourRequest       the tour Request
     * @param tourCompleteGraph the complete checkpoint graph
     */
    public TSPEvo(TourRequest tourRequest, TourCompleteGraph tourCompleteGraph) {
        super(tourRequest, tourCompleteGraph);
    }

    /**
     * Create an iterator to browse all unseen checkpoints.
     * Our strategy is to go to the nearest checkpoints from currentNode.
     *
     * @param currentNode  The node where we are currently in research
     * @param unseen       The unseen checkpoints
     * @param seenBoolean  The already seen checkpoints (in the form of a boolean array, no information of order)
     * @param cost         A 2d array of the cost to travel between each couple of checkpoints
     * @param predecessors An array that indicates if a checkpoint must be preceded by an other checkpoint
     * @return An iterator to travel as well as possible to all the checkpoints not yet seen
     */
    @Override
    protected Iterator<Integer> iterator(Integer currentNode, List<Integer> unseen, boolean[] seenBoolean, double[][] cost, int[] predecessors) {
        return new IteratorOptimized(currentNode, unseen, seenBoolean, cost, predecessors);
    }

    /**
     * Gives a lower bound of the distance to travel to see all unseen checkpoints.
     * Our strategy is: (bound=0 at the beginning)
     * - first, find the smallest path between the currentNode and all unseen nodes
     * (because we'll have to go from the current node to one of these nodes)
     * AND add the corresponding length to bound
     * - secondly, for each unseen node, find the smallest path between this node and all the
     * other unseen node as well as the end node AND add the corresponding length to bound
     * (because each unseen node will then go to an unseen node or the end)
     *
     * @param currentNode The node where we are currently in research
     * @param unseen      The unseen checkpoints
     * @param cost        A 2d array of the cost to travel between each couple of checkpoints
     * @return the lower bound mentioned above
     */
    @Override
    protected double bound(Integer currentNode, List<Integer> unseen, double[][] cost) {
        //bound that the functions will return:
        double minBound = Double.MAX_VALUE;

        //We search the smallest length between the currentNode and all unseen nodes:
        double dist;
        for (Integer node : unseen) {
            dist = cost[currentNode][node];
            if (dist < minBound) {
                minBound = dist;
            }
        }

        //for each unseen node, we search the smallest length between this node and all the
        //other unseen node as well as the end node
        double minLocal;
        for (Integer node : unseen) {
            minLocal = Double.MAX_VALUE;
            for (Integer otherNode : unseen) {
                if (node != otherNode) {
                    dist = cost[node][otherNode];
                    if (dist < minLocal) {
                        minLocal = dist;
                    }
                }
            }
            dist = cost[node][0];
            if (dist < minLocal) {
                minLocal = dist;
            }
            //We add the minimal length to the bound:
            minBound += minLocal;
        }

        return minBound;
    }
}
