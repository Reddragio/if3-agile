package com.insa.h4112.model.domain;

/**
 * All checkpoints types
 * @author Pierre-Yves GENEST
 */
public enum CheckpointType {
    WAREHOUSE,      // If checkpoint is associated to a warehouse
    PICK_UP,        // If checkpoint is a pick-up point
    DELIVER         // If checkpoint is a deliver point
}
