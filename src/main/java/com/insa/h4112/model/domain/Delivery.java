package com.insa.h4112.model.domain;

import java.util.Objects;

/**
 * Delivery of a parcel : pick-up and delivery points
 * (fr : Livraison)
 *
 * @author Paul GOUX
 */
public class Delivery {

    /**
     * Checkpoint where parcel is picked-up
     */
    private Checkpoint pickUp;

    /**
     * Checkpoint where parcel is delivered
     */
    private Checkpoint deliver;

    /**
     * Complete constructor
     *
     * @param pickUp  pick-up point
     * @param deliver delivery point
     */
    public Delivery(Checkpoint pickUp, Checkpoint deliver) {
        this.pickUp = pickUp;
        this.deliver = deliver;
    }

    /**
     * Accessor
     *
     * @return delivery checkpoint
     */
    public Checkpoint getDeliver() {
        return deliver;
    }

    /**
     * Setter
     *
     * @param deliver delivery checkpoint
     */
    public void setDeliver(Checkpoint deliver) {
        this.deliver = deliver;
    }

    /**
     * Accessor
     *
     * @return pick-up checkpoint
     */
    public Checkpoint getPickUp() {
        return pickUp;
    }

    /**
     * Setter
     *
     * @param pickUp pick-up checkpoint
     */
    public void setPickUp(Checkpoint pickUp) {
        this.pickUp = pickUp;
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Delivery
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Delivery delivery = (Delivery) o;
        return Objects.equals(pickUp, delivery.pickUp) &&
                Objects.equals(deliver, delivery.deliver);
    }

    /**
     * Hash delivery
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(pickUp, deliver);
    }

    /**
     * Generate a seed for color generator
     * /!\ Use this to generate color
     *
     * @return seed
     */
    public int getSeed() {
        return (int) Math.min(pickUp.getId(), deliver.getId());
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Delivery{" +
                "pickUp=" + pickUp +
                ", deliver=" + deliver +
                '}';
    }
}
