package com.insa.h4112.model.domain;

import com.insa.h4112.model.algorithm.BasicPair;
import com.insa.h4112.model.algorithm.SortPair;
import com.insa.h4112.util.CoordinatesConversion;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Road section that link an origin intersection to a destination intersection
 * (fr : Troncon)
 *
 * @author Paul GOUX
 * @author Jacques CHARNAY
 */
public class Section {

    /**
     * Origin intersection
     */
    private Intersection origin;

    /**
     * Destination intersection
     */
    private Intersection destination;

    /**
     * Road name
     */
    private String streetName;

    /**
     * Length of section (in meter)
     */
    private double length;

    /**
     * {@link Section} are oriented road, of more mathematically an arc
     *
     * @param origin      {@link Intersection} of origin
     * @param destination {@link Intersection} of destination
     * @param streetName  name of the street
     * @param length      length of the road
     */
    public Section(Intersection origin, Intersection destination, String streetName, double length) {
        this.origin = origin;
        this.destination = destination;
        this.streetName = streetName;
        this.length = length;
    }

    /**
     * Accessor
     *
     * @return origin intersection
     */
    public Intersection getOrigin() {
        return origin;
    }

    /**
     * Setter
     *
     * @param origin origin intersection
     */
    public void setOrigin(Intersection origin) {
        this.origin = origin;
    }

    /**
     * Accessor
     *
     * @return destination intersection
     */
    public Intersection getDestination() {
        return destination;
    }

    /**
     * Setter
     *
     * @param destination destination intersection
     */
    public void setDestination(Intersection destination) {
        this.destination = destination;
    }

    /**
     * Accessor
     *
     * @return street name
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Setter
     *
     * @param streetName street name
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * Accessor
     *
     * @return section length (in meter)
     */
    public double getLength() {
        return length;
    }

    /**
     * Setter
     *
     * @param length section length (in meter)
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * Indicate the direction (radian angle) to follow to reach next section
     * /!\ the nextSection must be connected to the currentSection
     * for the function to make sense
     *
     * @param nextSection next Section
     * @return the direction (radian angle) to follow
     */
    private double getAngleRelativeToNextSection(Section nextSection) {
        //First, we convert all the coordinnates of the section and nextSection from LatLng to XY plan:
        Point2D.Double XYDestination = CoordinatesConversion.latLngToXY(destination.getLatitude(), destination.getLongitude());
        XYDestination.y *= -1.0;
        Point2D.Double XYOrigin = CoordinatesConversion.latLngToXY(origin.getLatitude(), origin.getLongitude());
        XYOrigin.y *= -1.0;
        Point2D.Double XYNextDestination = CoordinatesConversion.latLngToXY(nextSection.destination.getLatitude(), nextSection.destination.getLongitude());
        XYNextDestination.y *= -1.0;
        Point2D.Double XYNextOrigin = CoordinatesConversion.latLngToXY(nextSection.origin.getLatitude(), nextSection.origin.getLongitude());
        XYNextOrigin.y *= -1.0;

        //We build the unitary vectors tangent to the actual section and next section
        // They muss have the same origin. That's wy we "invert" the sens for the tangent vector of current section
        Point2D.Double vectorCurrent = new Point2D.Double(XYOrigin.x - XYDestination.x, XYOrigin.y - XYDestination.y);
        Point2D.Double vectorNext = new Point2D.Double(XYNextDestination.x - XYNextOrigin.x, XYNextDestination.y - XYNextOrigin.y);

        //Then, we calculate the polar coordinate theta for each vector:
        double angleCurrent = Math.atan2(vectorCurrent.y, vectorCurrent.x);
        double angleNext = Math.atan2(vectorNext.y, vectorNext.x);
        //We eventually add 2*pi to angleNext in order to have always a positive result later:
        if (angleNext < angleCurrent) {
            angleNext += 2 * Math.PI;
        }

        //We deduce the direction to follow from the angle formed by the 2 sections:
        return angleNext - angleCurrent;

    }

    /**
     * Indicate the precise direction to follow to reach next section
     * /!\ the nextSection must be connected to the currentSection
     * for the function to make sense
     *
     * @param nextSection next Section
     * @return The precise direction to follow
     */
    public PreciseDirection getPreciseDirectionToNextSection(Section nextSection) {
        //First, we calculate for each following section the angle formed relatively to the current section
        List<SortPair<Double, Section>> allNextSections = new ArrayList<>();
        for (Section outgoingSection : destination.getOutgoingSections()) {
            if (outgoingSection.getDestination() != this.origin) {
                allNextSections.add(new SortPair<>(getAngleRelativeToNextSection(outgoingSection), outgoingSection));
            }
        }
        //We sort this angles by ascending order (namely, to have them from the right to the top left)
        Collections.sort(allNextSections);

        //Then, for each following section, we calculate the corresponding basic direction:
        List<BasicPair<Section, Direction>> directionForEachSection = new ArrayList<>();
        double angleBetweenSections;
        Direction direction, goalDirection = Direction.AHEAD;
        int nbLeft = 0, nbRight = 0;
        for (SortPair<Double, Section> pair : allNextSections) {
            angleBetweenSections = pair.getFirst();
            if (angleBetweenSections <= 4 * Math.PI / 5) {
                direction = Direction.RIGHT;
                ++nbRight;
            } else if (angleBetweenSections < 6 * Math.PI / 5) {
                direction = Direction.AHEAD;
            } else {
                direction = Direction.LEFT;
                ++nbLeft;
            }
            directionForEachSection.add(new BasicPair<>(pair.getSecond(), direction));

            if (pair.getSecond() == nextSection) {
                goalDirection = direction;
            }
        }

        //Finally, knowing the direction of the section where we want to go,
        //we can deduce the precise direction, namely the basic direction
        //AND eventually the exit number, if there is several exits:
        if (goalDirection == Direction.RIGHT) {
            if (nbRight == 1) {
                return new PreciseDirection(Direction.RIGHT, -1);
            } else {
                for (int i = 0; i < nbRight; ++i) {
                    if (directionForEachSection.get(i).getFirst() == nextSection) {
                        return new PreciseDirection(Direction.RIGHT, i + 1);
                    }
                }
            }
        } else if (goalDirection == Direction.LEFT) {
            if (nbLeft == 1) {
                return new PreciseDirection(Direction.LEFT, -1);
            } else {
                for (int i = 0; i < nbLeft; ++i) {
                    if (directionForEachSection.get(directionForEachSection.size() - (i + 1)).getFirst() == nextSection) {
                        return new PreciseDirection(Direction.LEFT, i + 1);
                    }
                }
            }
        }
        return new PreciseDirection(Direction.AHEAD, -1);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return Double.compare(section.length, length) == 0 &&
                Objects.equals(origin, section.origin) &&
                Objects.equals(destination, section.destination) &&
                Objects.equals(streetName, section.streetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(origin, destination, streetName, length);
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Section{" +
                "originId=" + origin.getId() +
                ", destinationId=" + destination.getId() +
                ", streetName='" + streetName + '\'' +
                ", length=" + length +
                '}';
    }
}
