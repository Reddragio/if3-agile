package com.insa.h4112.model.domain;

import com.insa.h4112.controller.Events;
import com.insa.h4112.model.algorithm.TourCompleteGraph;

import java.beans.PropertyChangeSupport;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Ordered list of paths that link warehouse to each checkpoint (respecting precedence rules)
 * and then return to warehouse
 * (fr : Tournee)
 *
 * @author Paul GOUX
 * @author Jacques CHARNAY
 * @author Martin FRANCESCHI
 */
public class Tour {

    /**
     * Ordered sequence of paths
     */
    private List<Path> paths;

    /**
     * Ordered sequence of checkpoints
     * For the sake of convenience, it doesn't contains the begin/end 2 checkpoints from warehouse
     * There is a direct correspondence between paths and checkpoints:
     * For every i, paths[i] goes to checkpoints[i]
     * For every i, paths[i+1] leaves from checkpoints[i]
     */
    private List<Checkpoint> checkpoints;

    /**
     * the tour Request
     */
    private TourRequest tourRequest;

    /**
     * the tour complete graph
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * Used to notify changes to view
     */
    PropertyChangeSupport propertyChangeSupport;

    /**
     * Complete constructor
     *
     * @param paths ordered sequence of paths
     */
    public Tour(List<Path> paths, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph) {
        this.paths = paths;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
        propertyChangeSupport = new PropertyChangeSupport(this);

        checkpoints = new ArrayList<>();
        for (int i = 1; i < paths.size(); ++i) {
            checkpoints.add(paths.get(i).getOrigin());
        }

        updatePassageTimes(0);
    }

    /**
     * Indicate if we can move a checkpoint from a position in list to an other one
     *
     * @param from Index of the checkpoint to move
     * @param to   Index where the moved checkpoint must go
     * @return move allowed or not
     */
    public boolean getMoveCheckpointFromToAllowed(int from, int to) {
        if (!(from >= 0 && from < checkpoints.size() && to >= 0 && to <= checkpoints.size())) {
            return false;
        }

        Checkpoint checkpointToMove = checkpoints.get(from);
        int relatedCheckpointIndex = checkpoints.indexOf(tourRequest.getOtherCheckpointInDelivery(checkpointToMove));
        boolean moveForbidden = (tourRequest.getCheckpointType(checkpointToMove) == CheckpointType.PICK_UP && relatedCheckpointIndex < to);
        moveForbidden = moveForbidden || (tourRequest.getCheckpointType(checkpointToMove) == CheckpointType.DELIVER && relatedCheckpointIndex >= to);
        if (moveForbidden) {
            return false;
        }
        return true;
    }

    /**
     * Change the order of passage of checkpoints, by moving a checkpoint from a position in list to an other one
     *
     * @param from Index of the checkpoint to move
     * @param to   Index where the moved checkpoint must go
     * @return true if the checkpoint has been moved, false otherwise
     */
    public boolean moveCheckpointFromTo(int from, int to) {
        if (!getMoveCheckpointFromToAllowed(from, to)) {
            return false;
        }

        Checkpoint checkpointToMove = checkpoints.get(from);
        checkpoints.add(to, checkpointToMove);
        if (to <= from) {
            checkpoints.remove(from + 1);
        } else {
            checkpoints.remove(from);
        }

        Path pathBefore = paths.remove(from);
        Path pathAfter = paths.remove(from);
        paths.add(from, tourCompleteGraph.getPathBTWCheckpoints(pathBefore.getOrigin(), pathAfter.getDestination()));
        if (to > from) {
            to -= 1;
        }
        Path pathInsertBetween = paths.remove(to);
        paths.add(to, tourCompleteGraph.getPathBTWCheckpoints(checkpointToMove, pathInsertBetween.getDestination()));
        paths.add(to, tourCompleteGraph.getPathBTWCheckpoints(pathInsertBetween.getOrigin(), checkpointToMove));

        updatePassageTimes(Math.min(from, to));

        propertyChangeSupport.firePropertyChange(Events.REORDER_CHECKPOINT_EVENT, null, this);

        return true;
    }

    /**
     * check if a delivery can be add at the specified positions
     *
     * @param delivery     delivery to add
     * @param indexPickUp  index to where add pickup
     * @param indexDeliver index to where add deliver
     * @return true if the delivery can be add, false otherwise
     */
    public boolean getAddDeliveryAllowed(Delivery delivery, int indexPickUp, int indexDeliver) {
        return (indexPickUp <= indexDeliver) &&
                (0 <= indexPickUp) &&
                (indexPickUp <= checkpoints.size()) &&
                (0 <= indexDeliver) &&
                (indexDeliver <= checkpoints.size()) &&
                tourCompleteGraph.verifyCheckpointReachableFromWarehouse(tourRequest.getWarehouse(), delivery.getPickUp()) &&
                tourCompleteGraph.verifyCheckpointReachableFromWarehouse(tourRequest.getWarehouse(), delivery.getDeliver());
    }

    /**
     * Add delivery to the tour
     *
     * @param delivery     delivery to add
     * @param indexPickUp  index to where add pickup
     * @param indexDeliver index to where add deliver
     * @return true of the delivery has been added, false otherwise
     */
    public boolean addDelivery(Delivery delivery, int indexPickUp, int indexDeliver) {
        if (!getAddDeliveryAllowed(delivery, indexPickUp, indexDeliver)) {
            return false;
        }

        Checkpoint pickUp = delivery.getPickUp();
        Checkpoint deliver = delivery.getDeliver();
        tourRequest.addDelivery(delivery);
        tourCompleteGraph.addDelivery(delivery);

        addCheckpointToListAndPaths(indexPickUp, pickUp);
        indexDeliver += 1;
        addCheckpointToListAndPaths(indexDeliver, deliver);

        updatePassageTimes(Math.min(indexPickUp, indexDeliver));
        //TODO : Catch the property change inside view
        propertyChangeSupport.firePropertyChange(Events.ADD_DELIVERY_FROM_TOUR_EVENT, null, this);

        return true;
    }

    /**
     * add a checkpoint to the different private objects
     *
     * @param indexCheckpoint index of the checkpoint to add
     * @param checkpoint      checkpoint to add
     */
    private void addCheckpointToListAndPaths(int indexCheckpoint, Checkpoint checkpoint) {
        Path deletedPath;
        Checkpoint checkpointBefore;
        Checkpoint checkpointAfter;
        checkpoints.add(indexCheckpoint, checkpoint);
        deletedPath = paths.get(indexCheckpoint);
        checkpointBefore = deletedPath.getOrigin();
        checkpointAfter = deletedPath.getDestination();
        paths.remove(indexCheckpoint);
        paths.add(indexCheckpoint, tourCompleteGraph.getPathBTWCheckpoints(checkpoint, checkpointAfter));
        paths.add(indexCheckpoint, tourCompleteGraph.getPathBTWCheckpoints(checkpointBefore, checkpoint));
    }

    /**
     * check if
     *
     * @param indexCheckpoint
     * @return
     */
    public boolean getRemoveDeliveryOfCheckpointAllowed(int indexCheckpoint) {
        return (indexCheckpoint >= 0) &&
                (indexCheckpoint < checkpoints.size());
    }

    public boolean removeDeliveryOfCheckpoint(int indexCheckpoint) {
        if (!getRemoveDeliveryOfCheckpointAllowed(indexCheckpoint)) {
            return false;
        }

        Checkpoint deletedCheckpoint = checkpoints.get(indexCheckpoint);
        Checkpoint otherDeletedCheckpoint = tourRequest.getOtherCheckpointInDelivery(deletedCheckpoint);
        int indexOtherCheckpoint = checkpoints.indexOf(otherDeletedCheckpoint);

        removeCheckpointFromListAndPaths(indexCheckpoint);
        if (indexOtherCheckpoint > indexCheckpoint) {
            //If the otherDeletedCheckpoint is actually a deliver
            indexOtherCheckpoint -= 1;
        }
        removeCheckpointFromListAndPaths(indexOtherCheckpoint);

        Delivery associatedDelivery = tourRequest.getAssociatedDelivery(deletedCheckpoint);
        tourRequest.removeDelivery(associatedDelivery);
        tourCompleteGraph.removeDelivery(associatedDelivery);

        updatePassageTimes(Math.min(indexCheckpoint, indexOtherCheckpoint));
        //TODO : Catch the property change inside view
        propertyChangeSupport.firePropertyChange(Events.REMOVE_DELIVERY_FROM_TOUR_EVENT, null, this);

        return true;
    }

    private void removeCheckpointFromListAndPaths(int indexCheckpoint) {
        checkpoints.remove(indexCheckpoint);
        Path pathBefore = paths.remove(indexCheckpoint);
        Path pathAfter = paths.remove(indexCheckpoint);
        paths.add(indexCheckpoint, tourCompleteGraph.getPathBTWCheckpoints(pathBefore.getOrigin(), pathAfter.getDestination()));
    }

    /**
     * Update all checkpoints passage time linked to paths from the index into paths list
     *
     * @param index
     */
    private void updatePassageTimes(int index) {
        LocalTime endPathTime;
        Path currentPath;
        Checkpoint origin;
        for (int i = index; i < paths.size(); ++i) {
            currentPath = paths.get(i);
            origin = currentPath.getOrigin();
            endPathTime = origin.getPassageTime();
            endPathTime = endPathTime.plus(origin.getDuration());
            endPathTime = endPathTime.plus(currentPath.getPathDuration());
            currentPath.getDestination().setPassageTime(endPathTime);
        }
    }

    /**
     * Getter
     *
     * @param index index of path
     *              <b>index not checked -> can throw OutOfBoundException</b>
     * @return path at index
     */
    public Path getPath(int index) {
        return paths.get(index);
    }

    /**
     * @return The first path from the list.
     */
    public Path getFirstPath() {
        return getPath(0);
    }

    /**
     * @return The last path from the list.
     */
    public Path getLastPath() {
        return getPath(paths.size() - 1);
    }

    /**
     * Return all {@link Section}s in this Tour. Possible duplicate values.
     *
     * @return List of {@link Section}s
     */
    public List<Section> getAllSectionsInPaths() {
        List<Section> sections = new ArrayList<>();
        for (Path path : paths) {
            sections.addAll(path.getSteps());
        }
        return sections;
    }

    /**
     * To get all paths in tour
     *
     * @return all paths
     */
    public List<Path> getAllPaths() {
        return paths;
    }

    /**
     * Gets all checkpoints, except the 2 links to the warehouse
     *
     * @return all checkpoints, except the 2 begin/end checkpoints from warehouse
     */
    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    /**
     * Gets all checkpoints.
     * The warehouse counts as two: one at the beginning and one at the end.
     *
     * @return all checkpoints.
     */
    public List<Checkpoint> getAllCheckpoints() {
        List<Checkpoint> allCheckpointsList = new ArrayList<>(checkpoints);
        allCheckpointsList.add(0, getFirstPath().getOrigin());
        allCheckpointsList.add(getLastPath().getDestination());
        return allCheckpointsList;
    }

    public TourRequest getTourRequest() {
        return tourRequest;
    }

    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Tour
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour tour = (Tour) o;
        return Objects.equals(paths, tour.paths);
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(paths);
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Tour{" +
                "paths=" + paths +
                '}';
    }
}
