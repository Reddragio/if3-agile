package com.insa.h4112.model.domain;

import java.time.LocalTime;
import java.util.Objects;

/**
 * A time interval with a start time and an end time
 *
 * @author Pierre-Yves GENEST
 */
public class TimeInterval {

    /**
     * Start of time interval
     */
    private LocalTime startTime;

    /**
     * End time of interval
     */
    private LocalTime endTime;

    /**
     * If startTime > endTime (comparison of LocalTime)
     * for instance : interval between 23h00-1h00 -> endGreaterThanStart = false
     * interval between 12h00-14h00 -> endGreaterThanStart = true
     */
    private boolean endGreaterThanStart;

    /**
     * default hour interval between startTime and endTime
     */
    private static final int HOUR_INTERVAL = 2;

    /**
     * Default constructor
     */
    public TimeInterval() {
        this(LocalTime.of(0, 0, 0), LocalTime.of(23, 59, 59));
    }

    /**
     * Constructor
     *
     * @param startTime center of interval
     * @param endTime   end time
     */
    public TimeInterval(LocalTime startTime, LocalTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;

        endGreaterThanStart = endTime.compareTo(startTime) >= 0;
    }

    /**
     * If other time is contained in this interval
     *
     * @param otherTime other time
     * @return if other time is contained in this interval
     */
    public boolean contains(LocalTime otherTime) {
        if (endGreaterThanStart) {
            return startTime.compareTo(otherTime) <= 0 && endTime.compareTo(otherTime) >= 0;
        } else {
            return (startTime.compareTo(otherTime) <= 0 && endTime.compareTo(otherTime) <= 0)
                    || (startTime.compareTo(otherTime) >= 0 && endTime.compareTo(otherTime) >= 0);
        }
    }

    /**
     * Compute an interval which is centered around middle time
     *
     * @param middleTime middle time
     * @return time interval
     */
    public static TimeInterval computeInterval(LocalTime middleTime) {
        int middleHour = middleTime.getHour();
        int minutes = (int) Math.round(middleTime.getMinute() / 15.) * 15;    // round to quarter

        if (minutes == 60) {
            middleHour = (middleHour + 1) % 24;
            minutes = 0;
        }

        return new TimeInterval(LocalTime.of((middleHour - HOUR_INTERVAL + 24) % 24, minutes), LocalTime.of((middleHour + HOUR_INTERVAL) % 24, minutes));
    }

    /**
     * Getter
     *
     * @return start time
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * Setter
     *
     * @return end time
     */
    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * Test equality
     *
     * @param o other object
     * @return if o and this are equal (value)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeInterval that = (TimeInterval) o;
        return Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    /**
     * Hash this object
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime);
    }

    /**
     * Description of this object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "TimeInterval{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
