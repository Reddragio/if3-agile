package com.insa.h4112.controller.command.util;

import com.insa.h4112.controller.command.Command;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandListTest {
    private int dos;
    private int undos;

    private CommandList commandList;

    @Test
    public void add() {
        commandList.add(new TestCommand(this));
        assertEquals(1, dos);
        assertEquals(0, undos);
        commandList.add(new TestCommand(this));
        assertEquals(2, dos);
        assertEquals(0, undos);
        commandList.add(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);
        commandList.add(new TestCommand(this));
        assertEquals(4, dos);
        assertEquals(0, undos);


        commandList.undo();
        commandList.undo();
        assertEquals(4, dos);
        assertEquals(2, undos);

        commandList.add(new TestCommand(this));
        assertEquals(5, dos);
        assertEquals(2, undos);
        commandList.redo();
        commandList.redo();
        assertEquals(5, dos);
        assertEquals(2, undos);

        commandList.undo();
        commandList.undo();
        commandList.undo();
        assertEquals(5, dos);
        assertEquals(5, undos);
        commandList.undo();
        assertEquals(5, dos);
        assertEquals(5, undos);

    }

    @Test
    public void redo() {
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        commandList.redo();
        commandList.redo();
        assertEquals(3, dos);
        assertEquals(0, undos);

        commandList.undo();
        commandList.undo();
        commandList.redo();
        assertEquals(4, dos);
        assertEquals(2, undos);
        commandList.redo();
        assertEquals(5, dos);
        assertEquals(2, undos);
        commandList.redo();
        commandList.redo();
        assertEquals(5, dos);
        assertEquals(2, undos);
    }

    @Test
    public void undo() {
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        commandList.undo();
        assertEquals(3, dos);
        assertEquals(1, undos);
        commandList.undo();
        commandList.undo();
        assertEquals(3, dos);
        assertEquals(3, undos);
        commandList.undo();
        commandList.undo();
        assertEquals(3, dos);
        assertEquals(3, undos);

        commandList.redo();
        commandList.redo();
        assertEquals(5, dos);
        assertEquals(3, undos);
        commandList.undo();
        commandList.undo();
        assertEquals(5, dos);
        assertEquals(5, undos);
        commandList.undo();
        commandList.undo();
        assertEquals(5, dos);
        assertEquals(5, undos);

    }

    @Test
    public void undoAndEraseCommand() {
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        commandList.undoAndEraseCommand();
        assertEquals(3, dos);
        assertEquals(1, undos);
        commandList.redo();
        assertEquals(3, dos);
        assertEquals(1, undos);

        commandList.undoAndEraseCommand();
        commandList.undoAndEraseCommand();
        assertEquals(3, dos);
        assertEquals(3, undos);

        commandList.redo();
        commandList.redo();
        assertEquals(3, dos);
        assertEquals(3, undos);
    }

    @Test
    public void clear() {
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        commandList.add(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        commandList.clear();
        commandList.undo();
        commandList.undo();
        commandList.redo();
        commandList.redo();
        assertEquals(3, dos);
        assertEquals(0, undos);
    }

    @Test
    public void isEverythingUndone() {
        assertTrue(commandList.isEverythingUndone());

        commandList.add(new TestCommand(this));
        assertFalse(commandList.isEverythingUndone());
        commandList.add(new TestCommand(this));
        assertFalse(commandList.isEverythingUndone());
        commandList.add(new TestCommand(this));
        assertFalse(commandList.isEverythingUndone());

        commandList.undo();
        assertFalse(commandList.isEverythingUndone());
        commandList.undo();
        assertFalse(commandList.isEverythingUndone());
        commandList.undo();
        assertTrue(commandList.isEverythingUndone());

        commandList.redo();
        assertFalse(commandList.isEverythingUndone());
        commandList.redo();
        assertFalse(commandList.isEverythingUndone());
        commandList.undo();
        assertFalse(commandList.isEverythingUndone());
        commandList.undo();
        assertTrue(commandList.isEverythingUndone());
    }

    @Before
    public void before() {
        dos = 0;
        undos = 0;
        commandList = new CommandList();
    }

    public void receiveDo() {
        dos++;
    }

    public void receiveUndo() {
        undos++;
    }

    private class TestCommand implements Command {
        private CommandListTest test;

        public TestCommand(CommandListTest test) {
            this.test = test;
        }

        @Override
        public void doCommand() {
            test.receiveDo();
        }

        @Override
        public void undoCommand() {
            test.receiveUndo();
        }
    }
}
