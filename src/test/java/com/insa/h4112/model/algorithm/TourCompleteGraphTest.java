package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Jacques CHARNAY
 */
public class TourCompleteGraphTest {
    private static TourCompleteGraph tourCompleteGraph;
    private static TourCompleteGraph tourCompleteGraphInvalid;
    private static TourCompleteGraph tourCompleteGraphInvalid2;
    private static TourCompleteGraph tourCompleteGraphEmpty;
    private static TourRequest tourRequest;
    private static TourRequest tourRequest2;
    private static TourRequest tourRequestEmpty;
    private static Map map;

    private static Intersection inter0 = new Intersection(0, 0.0, 0.0);
    private static Intersection inter1 = new Intersection(1, 0.0, 0.0);
    private static Intersection inter2 = new Intersection(2, 0.0, 0.0);
    private static Intersection inter3 = new Intersection(3, 0.0, 0.0);
    private static Intersection inter4 = new Intersection(4, 0.0, 0.0);
    private static Intersection inter5 = new Intersection(5, 0.0, 0.0);
    private static Intersection inter42 = new Intersection(42, 0.0, 0.0);

    private static Section se0_2 = new Section(inter0, inter2, "", 3.0);
    private static Section se2_3 = new Section(inter2, inter3, "", 4.0);
    private static Section se0_1 = new Section(inter0, inter1, "", 6.0);
    private static Section se1_0 = new Section(inter1, inter0, "", 6.0);
    private static Section se1_3 = new Section(inter1, inter3, "", 7.0);
    private static Section se3_1 = new Section(inter3, inter1, "", 7.0);
    private static Section se3_4 = new Section(inter3, inter4, "", 10.0);
    private static Section se4_5 = new Section(inter4, inter5, "", 2.0);
    private static Section se5_3 = new Section(inter5, inter3, "", 4.0);

    private static Checkpoint start = new Checkpoint(Duration.ofSeconds(0l), inter0);
    private static Checkpoint end = new Checkpoint(Duration.ofSeconds(0l), inter0);
    private static Checkpoint pickupPoint1 = new Checkpoint(Duration.ofSeconds(0l), inter5);
    private static Checkpoint deliverPoint1 = new Checkpoint(Duration.ofSeconds(0l), inter1);
    private static Checkpoint pickupPoint2 = new Checkpoint(Duration.ofSeconds(0l), inter42);
    private static Checkpoint deliverPoint2 = new Checkpoint(Duration.ofSeconds(0l), inter1);
    private static Checkpoint pickupPoint3 = new Checkpoint(Duration.ofSeconds(0l), inter2);
    private static Checkpoint deliverPoint3 = new Checkpoint(Duration.ofSeconds(0l), inter42);
    private static Delivery deliver1 = new Delivery(pickupPoint1, deliverPoint1);
    private static Delivery incorrectDelivery = new Delivery(pickupPoint2, deliverPoint2);
    private static Delivery incorrectDelivery2 = new Delivery(pickupPoint3, deliverPoint3);

    @BeforeClass
    public static void beforeTests() {
        inter0.addOutgoingSection(se0_2);
        inter0.addOutgoingSection(se0_1);
        inter1.addOutgoingSection(se1_0);
        inter1.addOutgoingSection(se1_3);
        inter2.addOutgoingSection(se2_3);
        inter3.addOutgoingSection(se3_1);
        inter3.addOutgoingSection(se3_4);
        inter4.addOutgoingSection(se4_5);
        inter5.addOutgoingSection(se5_3);

        HashMap<Long, Intersection> interMap = new HashMap<>();
        interMap.put(0l, inter0);
        interMap.put(1l, inter1);
        interMap.put(2l, inter2);
        interMap.put(3l, inter3);
        interMap.put(4l, inter4);
        interMap.put(5l, inter5);
        interMap.put(6l, inter42);
        map = new Map(interMap);

        Warehouse warehouse = new Warehouse(LocalTime.now(), start, end);
        List<Delivery> deliveries = new ArrayList<>();
        deliveries.add(deliver1);

        tourRequest = new TourRequest(warehouse, deliveries);
        tourCompleteGraph = new TourCompleteGraph(map, tourRequest);
        tourRequest.addDelivery(incorrectDelivery);
        tourCompleteGraphInvalid = new TourCompleteGraph(map, tourRequest);
        tourRequest2 = new TourRequest(warehouse, deliveries);
        tourRequest2.addDelivery(incorrectDelivery2);
        tourCompleteGraphInvalid2 = new TourCompleteGraph(map, tourRequest2);

        List<Delivery> deliveries2 = new ArrayList<>();
        tourRequestEmpty = new TourRequest(warehouse, deliveries2);
        tourCompleteGraphEmpty = new TourCompleteGraph(map, tourRequestEmpty);
    }

    @Test
    public void getDistBTWCheckpoints() {
        assertEquals(0.0, tourCompleteGraph.getDistBTWCheckpoints(start, end), 0.0);
        assertEquals(0.0, tourCompleteGraph.getDistBTWCheckpoints(end, start), 0.0);
        assertEquals(19.0, tourCompleteGraph.getDistBTWCheckpoints(start, pickupPoint1), 0.0);
        assertEquals(6.0, tourCompleteGraph.getDistBTWCheckpoints(start, deliverPoint1), 0.0);
        assertEquals(19.0, tourCompleteGraph.getDistBTWCheckpoints(end, pickupPoint1), 0.0);
        assertEquals(6.0, tourCompleteGraph.getDistBTWCheckpoints(end, deliverPoint1), 0.0);

        assertEquals(17.0, tourCompleteGraph.getDistBTWCheckpoints(pickupPoint1, start), 0.0);
        assertEquals(17.0, tourCompleteGraph.getDistBTWCheckpoints(pickupPoint1, end), 0.0);
        assertEquals(11.0, tourCompleteGraph.getDistBTWCheckpoints(pickupPoint1, deliverPoint1), 0.0);

        assertEquals(6.0, tourCompleteGraph.getDistBTWCheckpoints(deliverPoint1, start), 0.0);
        assertEquals(6.0, tourCompleteGraph.getDistBTWCheckpoints(deliverPoint1, end), 0.0);
        assertEquals(19.0, tourCompleteGraph.getDistBTWCheckpoints(deliverPoint1, pickupPoint1), 0.0);
    }

    @Test
    public void getPathBTWCheckpoints() {
        List<Section> sectionPath0_5 = new ArrayList<>();
        sectionPath0_5.add(se0_2);
        sectionPath0_5.add(se2_3);
        sectionPath0_5.add(se3_4);
        sectionPath0_5.add(se4_5);
        Path path0_5 = new Path(start, pickupPoint1, sectionPath0_5);
        assertEquals(path0_5, tourCompleteGraph.getPathBTWCheckpoints(start, pickupPoint1));

        List<Section> sectionPath5_1 = new ArrayList<>();
        sectionPath5_1.add(se5_3);
        sectionPath5_1.add(se3_1);
        Path path5_1 = new Path(pickupPoint1, deliverPoint1, sectionPath5_1);
        assertEquals(path5_1, tourCompleteGraph.getPathBTWCheckpoints(pickupPoint1, deliverPoint1));

        List<Section> sectionPath1_0 = new ArrayList<>();
        sectionPath1_0.add(se1_0);
        Path path1_0 = new Path(deliverPoint1, end, sectionPath1_0);
        assertEquals(path1_0, tourCompleteGraph.getPathBTWCheckpoints(deliverPoint1, end));
    }

    @Test
    public void getCheckpointPredecessor() {
        assertEquals(null, tourCompleteGraph.getCheckpointPredecessor(start));
        assertEquals(start, tourCompleteGraph.getCheckpointPredecessor(end));
        assertEquals(null, tourCompleteGraph.getCheckpointPredecessor(pickupPoint1));
        assertEquals(pickupPoint1, tourCompleteGraph.getCheckpointPredecessor(deliverPoint1));
    }


    @Test
    public void getCheckpoints() {
        assertEquals(4, tourCompleteGraph.getCheckpoints().size());
    }

    @Test
    public void getDistances() {
        assertEquals(16, tourCompleteGraph.getDistances().size());
    }

    @Test
    public void getPaths() {
        assertEquals(16, tourCompleteGraph.getPaths().size());
    }

    @Test
    public void getPrecededBy() {
        assertEquals(4, tourCompleteGraph.getPrecededBy().size());
    }

    @Test
    public void verifyAllCheckpointsReachableFromWarehouse() {
        assertTrue(tourCompleteGraph.verifyAllCheckpointsReachableFromWarehouse(tourRequest.getWarehouse()));
        assertFalse(tourCompleteGraphInvalid.verifyAllCheckpointsReachableFromWarehouse(tourRequest.getWarehouse()));
        assertFalse(tourCompleteGraphInvalid2.verifyAllCheckpointsReachableFromWarehouse(tourRequest2.getWarehouse()));
    }

    @Test
    public void verifyCheckpointReachableFromWarehouse() {
        Warehouse warehouse = tourRequest.getWarehouse();
        assertTrue(tourCompleteGraphInvalid.verifyCheckpointReachableFromWarehouse(warehouse, pickupPoint1));
        assertTrue(tourCompleteGraphInvalid.verifyCheckpointReachableFromWarehouse(warehouse, deliverPoint1));
        assertFalse(tourCompleteGraphInvalid.verifyCheckpointReachableFromWarehouse(warehouse, pickupPoint2));
        assertTrue(tourCompleteGraphInvalid.verifyCheckpointReachableFromWarehouse(warehouse, deliverPoint2));
    }

    @Test
    public void addAndRemoveDelivery() {
        tourCompleteGraphEmpty.addDelivery(deliver1);
        assertEquals(19.0, tourCompleteGraphEmpty.getDistBTWCheckpoints(start, pickupPoint1), 0.0);
        assertEquals(6.0, tourCompleteGraphEmpty.getDistBTWCheckpoints(start, deliverPoint1), 0.0);
        assertEquals(4, tourCompleteGraphEmpty.getCheckpoints().size());
        assertEquals(16, tourCompleteGraphEmpty.getDistances().size());
        assertEquals(16, tourCompleteGraphEmpty.getPaths().size());
        tourCompleteGraphEmpty.removeDelivery(deliver1);
        assertEquals(2, tourCompleteGraphEmpty.getCheckpoints().size());
        assertEquals(4, tourCompleteGraphEmpty.getDistances().size());
        assertEquals(4, tourCompleteGraphEmpty.getPaths().size());
    }
}