package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Section;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Jacques CHARNAY
 */
public class DijkstraDistanceMapTest {
    private static DijkstraDistanceMap cdMapFrom0;
    private static DijkstraDistanceMap cdMapFrom4;
    private static Map map;

    private static Intersection inter0 = new Intersection(0, 0.0, 0.0);
    private static Intersection inter1 = new Intersection(1, 0.0, 0.0);
    private static Intersection inter2 = new Intersection(2, 0.0, 0.0);
    private static Intersection inter3 = new Intersection(3, 0.0, 0.0);
    private static Intersection inter4 = new Intersection(4, 0.0, 0.0);

    private static Section se0_2 = new Section(inter0, inter2, "", 3.0);
    private static Section se2_3 = new Section(inter2, inter3, "", 4.0);
    private static Section se0_1 = new Section(inter0, inter1, "", 6.0);
    private static Section se1_0 = new Section(inter1, inter0, "", 6.0);
    private static Section se1_3 = new Section(inter1, inter3, "", 7.0);
    private static Section se3_1 = new Section(inter3, inter1, "", 7.0);
    private static Section se3_4 = new Section(inter3, inter4, "", 10.0);

    @BeforeClass
    public static void beforeTests() {
        inter0.addOutgoingSection(se0_2);
        inter0.addOutgoingSection(se0_1);
        inter1.addOutgoingSection(se1_0);
        inter1.addOutgoingSection(se1_3);
        inter2.addOutgoingSection(se2_3);
        inter3.addOutgoingSection(se3_1);
        inter3.addOutgoingSection(se3_4);

        HashMap<Long, Intersection> interMap = new HashMap<>();
        interMap.put(0l, inter0);
        interMap.put(1l, inter1);
        interMap.put(2l, inter2);
        interMap.put(3l, inter3);
        interMap.put(4l, inter4);
        map = new Map(interMap);

        cdMapFrom0 = new DijkstraDistanceMap(map, inter0, new HashSet<Intersection>());
        cdMapFrom4 = new DijkstraDistanceMap(map, inter4, new HashSet<Intersection>());
    }

    @Test
    public void getterTests() {
        java.util.Map<Intersection, Double> distances = cdMapFrom0.getDistances();
        Assert.assertEquals(0.0, distances.get(inter0), 0.0);

        java.util.Map<Intersection, Intersection> predecessorsIntersection = cdMapFrom0.getPredecessorsIntersection();
        Assert.assertEquals(null, predecessorsIntersection.get(inter0));
        Assert.assertEquals(inter0, predecessorsIntersection.get(inter2));
        Assert.assertEquals(inter2, predecessorsIntersection.get(inter3));
        Assert.assertEquals(inter3, predecessorsIntersection.get(inter4));

        java.util.Map<Intersection, Section> predecessorsSection = cdMapFrom0.getPredecessorsSection();
        Assert.assertEquals(null, predecessorsSection.get(inter0));
        Assert.assertEquals(se0_2, predecessorsSection.get(inter2));
        Assert.assertEquals(se2_3, predecessorsSection.get(inter3));
        Assert.assertEquals(se3_4, predecessorsSection.get(inter4));
    }

    @Test
    public void getOrigin() {
        Assert.assertEquals(inter0, cdMapFrom0.getOrigin());
        Assert.assertEquals(inter4, cdMapFrom4.getOrigin());
    }

    @Test
    public void distanceToInter() {
        Assert.assertEquals(0.0, cdMapFrom0.distanceToInter(inter0), 0.0);
        Assert.assertEquals(6.0, cdMapFrom0.distanceToInter(inter1), 0.0);
        Assert.assertEquals(3.0, cdMapFrom0.distanceToInter(inter2), 0.0);
        Assert.assertEquals(7.0, cdMapFrom0.distanceToInter(inter3), 0.0);
        Assert.assertEquals(17.0, cdMapFrom0.distanceToInter(inter4), 0.0);

        Assert.assertEquals(Double.MAX_VALUE, cdMapFrom4.distanceToInter(inter0), 0.0);
        Assert.assertEquals(Double.MAX_VALUE, cdMapFrom4.distanceToInter(inter1), 0.0);
        Assert.assertEquals(Double.MAX_VALUE, cdMapFrom4.distanceToInter(inter2), 0.0);
        Assert.assertEquals(Double.MAX_VALUE, cdMapFrom4.distanceToInter(inter3), 0.0);
        Assert.assertEquals(0.0, cdMapFrom4.distanceToInter(inter4), 0.0);
    }

    @Test
    public void getPathTo() {
        ArrayList<Section> path0_4 = new ArrayList<>();
        path0_4.add(se0_2);
        path0_4.add(se2_3);
        path0_4.add(se3_4);

        Assert.assertTrue(cdMapFrom0.getPathTo(inter4).equals(path0_4));
    }
}