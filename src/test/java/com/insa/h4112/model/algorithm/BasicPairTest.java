package com.insa.h4112.model.algorithm;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Jacques CHARNAY
 */
public class BasicPairTest {
    private static BasicPair<Integer, Integer> pair;

    @Before
    public void beforeEachTest() {
        pair = new BasicPair<>(4, 7);
    }

    @Test
    public void getFirst() {
        assertEquals((Integer) 4, pair.getFirst());
    }

    @Test
    public void setFirst() {
        pair.setFirst(2);
        assertEquals((Integer) 2, pair.getFirst());
    }

    @Test
    public void getSecond() {
        assertEquals((Integer) 7, pair.getSecond());
    }

    @Test
    public void setSecond() {
        pair.setSecond(8);
        assertEquals((Integer) 8, pair.getSecond());
    }
}