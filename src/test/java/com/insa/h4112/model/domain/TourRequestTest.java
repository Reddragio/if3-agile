package com.insa.h4112.model.domain;

import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class TourRequestTest {
    private static Map map;
    private static TourRequest tourRequest;
    private static Checkpoint start1;
    private static Checkpoint end1;
    private static Checkpoint checkpointPickUp;
    private static Checkpoint checkpointDeliver;
    private static Delivery delivery1;
    private static Warehouse warehouse;

    @Before
    public void beforeTests() {
        map = new Map(new HashMap<>());
        start1 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        end1 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        warehouse = new Warehouse(LocalTime.MIN, start1, end1);
        checkpointPickUp = new Checkpoint(Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        checkpointDeliver = new Checkpoint(Duration.ofSeconds(5), new Intersection(2l, 3.0, 3.0));
        delivery1 = new Delivery(checkpointPickUp, checkpointDeliver);

        List<Delivery> deliveryList = new ArrayList<>();
        deliveryList.add(delivery1);
        tourRequest = new TourRequest(warehouse, deliveryList);
    }

    @Test
    public void getOtherCheckpointInDelivery() {
        assertEquals(start1, tourRequest.getOtherCheckpointInDelivery(end1));
        assertEquals(end1, tourRequest.getOtherCheckpointInDelivery(start1));
        assertEquals(checkpointPickUp, tourRequest.getOtherCheckpointInDelivery(checkpointDeliver));
        assertEquals(checkpointDeliver, tourRequest.getOtherCheckpointInDelivery(checkpointPickUp));
    }

    @Test
    public void getCheckpointType() {
        assertEquals(CheckpointType.WAREHOUSE, tourRequest.getCheckpointType(start1));
        assertEquals(CheckpointType.WAREHOUSE, tourRequest.getCheckpointType(end1));
        assertEquals(CheckpointType.PICK_UP, tourRequest.getCheckpointType(checkpointPickUp));
        assertEquals(CheckpointType.DELIVER, tourRequest.getCheckpointType(checkpointDeliver));
    }

    @Test
    public void getDelivery() {
        Delivery delivery2 = tourRequest.getDelivery(0);
        assertEquals(checkpointPickUp, delivery2.getPickUp());
        assertEquals(checkpointDeliver, delivery2.getDeliver());
    }

    @Test
    public void removeDelivery() {
        tourRequest.removeDelivery(0);
        assertEquals(0, tourRequest.getDeliveries().size());
        tourRequest.addDelivery(delivery1);
        assertEquals(1, tourRequest.getDeliveries().size());
    }

    @Test
    public void getPropertyChangeSupport() {
        assertNotEquals(null, tourRequest.getPropertyChangeSupport());
    }

    @Test
    public void testHashCode() {
        TourRequest tourRequest2 = new TourRequest(warehouse, new ArrayList<>());
        assertFalse(tourRequest.hashCode() == tourRequest2.hashCode());
    }

    @Test
    public void testToString() {
        TourRequest tourRequest2 = new TourRequest(warehouse, new ArrayList<>());
        assertEquals("TourRequest", tourRequest2.toString().substring(0, 11));
    }
}