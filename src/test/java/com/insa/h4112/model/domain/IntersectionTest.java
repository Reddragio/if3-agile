package com.insa.h4112.model.domain;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Jacques CHARNAY
 */
public class IntersectionTest {
    private static Intersection intersection1;

    @BeforeClass
    public static void beforeTests() {
        intersection1 = new Intersection(0l, 0.0, 0.0, new ArrayList<Section>());
    }

    @Test
    public void setLatitude() {
        intersection1.setLatitude(7.0);
        assertEquals(7.0, intersection1.getLatitude(), 0.0);
    }

    @Test
    public void setLongitude() {
        intersection1.setLongitude(14.0);
        assertEquals(14.0, intersection1.getLongitude(), 0.0);
    }

    @Test
    public void setOutgoingSections() {
        List<Section> list1 = new ArrayList<Section>();
        Intersection intersection2 = new Intersection(1l, 0.0, 0.0);
        list1.add(new Section(intersection1, intersection2, "Street", 140.0));
        intersection1.setOutgoingSections(list1);
        assertEquals(1, intersection1.getOutgoingSections().size());
    }
}