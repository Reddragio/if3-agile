package com.insa.h4112.model.domain;

import org.junit.BeforeClass;
import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import java.time.Duration;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author Jacques CHARNAY
 */
public class CheckpointTest {
    private static Checkpoint checkpoint1;
    private static Checkpoint checkpoint2;
    private static Checkpoint checkpoint3;
    private static Checkpoint fakeCheckpoint = new Checkpoint(new Intersection(0L, 0.0, 0.0));

    @BeforeClass
    public static void beforeTests() {
        Inject.field("nextId").of(fakeCheckpoint).with(0);
        checkpoint1 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        checkpoint2 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        checkpoint3 = new Checkpoint(new Intersection(2l, 0.0, 0.0));
    }

    @Test
    public void setDuration() {
        checkpoint1.setDuration(Duration.ofSeconds(6));
        assertEquals(Duration.ofSeconds(6), checkpoint1.getDuration());
    }

    @Test
    public void setIntersection() {
        checkpoint1.setIntersection(new Intersection(4l, 0.0, 0.0));
        assertEquals(4l, checkpoint1.getIntersection().getId());
    }

    @Test
    public void getId() {
        assertEquals(0l, checkpoint1.getId());
    }

    @Test
    public void testToString() {
        String expectedString = "Checkpoint{id=1, passageTime=00:00, duration=PT3S, interval=TimeInterval{startTime=22:00, endTime=02:00}, intersection=Intersection{id=1, latitude=0.0, longitude=0.0, outgoingSections=[]}}";
        assertEquals(expectedString, checkpoint2.toString());
    }

    @Test
    public void setPassageTime() {
        checkpoint3.setPassageTime(LocalTime.MIDNIGHT);
        assertNotEquals(null, checkpoint3.getInterval());
    }

    @Test
    public void testSetDuration() {
        Duration duration1 = Duration.ofSeconds(7);
        checkpoint3.setDuration(duration1);
        assertEquals(duration1, checkpoint3.getDuration());
    }
}