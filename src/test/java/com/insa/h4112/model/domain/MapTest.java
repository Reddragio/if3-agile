package com.insa.h4112.model.domain;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MapTest {
    private static Map map;
    private static Map map2;
    private static Intersection intersection1;

    @BeforeClass
    public static void beforeTests() {
        map = new Map();
        map2 = new Map();
        intersection1 = new Intersection(0l, 0.0, 0.0);
    }

    @Test
    public void removeIntersection() {
        assertEquals(0, map.getAllIntersectionsAsList().size());
        map.addIntersection(intersection1);
        assertEquals(1, map.getAllIntersectionsAsList().size());
        assertEquals(intersection1, map.getIntersection(0l));
        map.removeIntersection(0l);
        assertEquals(0, map.getAllIntersectionsAsList().size());
    }

    @Test
    public void testHashCode() {
        assertTrue(map.hashCode() == map2.hashCode());
    }

    @Test
    public void testToString() {
        String ExpectedString = "Map{intersections={}}";
        assertEquals(ExpectedString, map.toString());
    }
}