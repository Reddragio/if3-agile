package com.insa.h4112.model.domain;

import org.junit.BeforeClass;
import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import java.time.Duration;

import static org.junit.Assert.*;

/**
 * @author Jacques CHARNAY
 */
public class DeliveryTest {
    private static Checkpoint checkpointPickUp;
    private static Checkpoint checkpointDeliver;
    private static Checkpoint checkpointPickUp2;
    private static Checkpoint checkpointDeliver2;
    private static Checkpoint fakeCheckpoint = new Checkpoint(new Intersection(0L, 0.0, 0.0));
    private static Delivery delivery1;
    private static Delivery delivery2;
    private static Delivery delivery3;

    @BeforeClass
    public static void beforeTests() {
        Inject.field("nextId").of(fakeCheckpoint).with(0);
        checkpointPickUp = new Checkpoint(Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        checkpointDeliver = new Checkpoint(Duration.ofSeconds(5), new Intersection(2l, 3.0, 3.0));
        checkpointPickUp2 = new Checkpoint(Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        checkpointDeliver2 = new Checkpoint(Duration.ofSeconds(5), new Intersection(2l, 3.0, 3.0));
        delivery1 = new Delivery(checkpointPickUp, checkpointDeliver);
        delivery2 = new Delivery(checkpointPickUp, checkpointDeliver);
        delivery3 = new Delivery(checkpointPickUp, checkpointDeliver);
    }

    @Test
    public void setDeliver() {
        delivery1.setDeliver(checkpointDeliver2);
        assertEquals(checkpointDeliver2, delivery1.getDeliver());
    }

    @Test
    public void setPickUp() {
        delivery1.setPickUp(checkpointPickUp2);
        assertEquals(checkpointPickUp2, delivery1.getPickUp());
    }

    @Test
    public void testHashCode() {
        delivery1.setPickUp(checkpointPickUp2);
        assertTrue(delivery2.hashCode() == delivery3.hashCode());
        assertFalse(delivery1.hashCode() == delivery2.hashCode());
        assertFalse(delivery1.hashCode() == delivery3.hashCode());
    }

    @Test
    public void getSeed() {
        assertEquals(0, delivery2.getSeed());
    }

    @Test
    public void testToString() {
        String expectedString = "Delivery{pickUp=Checkpoint{id=0, passageTime=null, duration=PT3S, interval=null, intersection=Intersection{id=1, latitude=0.0, longitude=0.0, outgoingSections=[]}}, deliver=Checkpoint{id=1, passageTime=null, duration=PT5S, interval=null, intersection=Intersection{id=2, latitude=3.0, longitude=3.0, outgoingSections=[]}}}";
        assertEquals(expectedString, delivery2.toString());
    }
}