package com.insa.h4112.model.parser;

import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Section;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * @author Paul Goux & Martin Franceschi
 */
public class MapXMLParserTest {

    public static final String DIR_PATH = "src" + File.separator + "test" + File.separator + "ressources" + File.separator + "parser" + File.separator + "map" + File.separator;

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_ElementNameInvalidIntersection() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_ElementNameInvalidIntersection.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_ElementNameInvalidSection() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_ElementNameInvalidSection.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_IntersectionsSameId() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_IntersectionsSameId.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_IntersectionsSamePosition() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_IntersectionsSamePosition.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_NoID() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_NoID.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_NoLatitude() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_NoLatitude.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_NoLongitude() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_NoLongitude.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_RootName() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_RootName.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_RootNumber() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_RootNumber.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_SectionInterSectionNonExistent() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_SectionInterSectionNon-existent.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_SectionNegativeLength() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_SectionNegativeLength.xml"));
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidMap_SectionNullLength() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "InvalidMap_SectionNullLength.xml"));
    }

    @Test
    public void ValidMap_DoubleValueStreetName() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_DoubleValueStreetName.xml"));
    }

    @Test
    public void ValidMap_EmptyValueStreetName() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_EmptyValueStreetName.xml"));
    }

    @Test
    public void ValidMap_Minimal() throws JDOMException, IOException {
        Map map = MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_Minimal.xml"));
        Map expectedMap = new Map();

        Intersection i1 = new Intersection(1, 1.1, 1.2);
        Intersection i2 = new Intersection(2, 2.1, 2.2);
        Intersection i3 = new Intersection(3, 3.1, 3.2);
        Intersection i4 = new Intersection(4, 4.1, 4.2);
        Intersection i5 = new Intersection(5, 5.1, 5.2);

        Section s1 = new Section(i2, i1, "RueDu10.2", 10.2);
        Section s2 = new Section(i1, i2, "RueDu20.1", 20.1);
        Section s3 = new Section(i4, i3, "RueDu30.4", 30.4);
        Section s4 = new Section(i5, i4, "RueDu40.5", 40.5);
        Section s5 = new Section(i5, i1, "RueDu10.5", 10.5);

        i1.addOutgoingSection(s2);
        i2.addOutgoingSection(s1);
        i4.addOutgoingSection(s3);
        i5.addOutgoingSection(s4);
        i5.addOutgoingSection(s5);

        expectedMap.addIntersection(i1);
        expectedMap.addIntersection(i2);
        expectedMap.addIntersection(i3);
        expectedMap.addIntersection(i4);
        expectedMap.addIntersection(i5);

        assertEquals(expectedMap, map);
    }

    @Test
    public void ValidMap_ProvidedBig() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_ProvidedBig.xml"));
    }

    @Test
    public void ValidMap_ProvidedMedium() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_ProvidedMedium.xml"));
    }

    @Test
    public void ValidMap_ProvidedSmall() throws JDOMException, IOException {
        MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_ProvidedSmall.xml"));
    }
}